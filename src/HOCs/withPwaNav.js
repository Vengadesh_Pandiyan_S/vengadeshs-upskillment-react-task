import React from "react";
import { makeStyles } from "@mui/styles";
import { TopNavBar, SideNavBar } from '../components';
import {Container} from "@mui/material"
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "100%",
    padding:"0px"

  },
  content: {
    width: '100%',
    height: 'calc(100% - 64px)',
    overflow: "auto",
  },
  topNavbar: {
    
    
  },
  sideNavbar: {
    [theme.breakpoints.down('md')]: {
      display: "none"
    }
  }
}));


const withNavBarsPWA = (Component) => (props) => {
  const classes = useStyles({ props });

  return (
    <Container maxWidth="sm" className={classes.root}>
      {/* Your nav bars here */}
      <div className={classes.topNavbar}>
        <TopNavBar />
      </div>

      {/* Content */}
      <div className={classes.content}>
        <Component {...props}>{props.children}</Component>
      </div>
    </Container>
  );
};

export default withNavBarsPWA;
