import React from "react";
import {
  AlertContext
} from "../contexts";
const withAllContexts = (Component) => (props) => {
  const alert = React.useContext(AlertContext);
  return (
    <Component
      {...props}
      alert={alert}
    >
      {props.children}
    </Component>
  );
};
export default withAllContexts;