import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { AppRoutes } from "./routes";
import PrivateRouter from "./privateRouter";

import {
  NotFound,
  Home,
  Login,
  PropertyDashboard,
  Collabo,
  PropertyCreation,
  Properties,
  PropertiesDash,
  PropertyView,
  PropertyViewMob,
  PersonalForm,
  PersonalFormTable,
  PropertyCreationMob,
  PropertiesDashMob,
  PropertyTable
} from './../screens';



const RouterApp = (props) => {
  
  return (
    <BrowserRouter>
      <Routes>

        {/* Home Route */}
        <Route path={AppRoutes.home} element={
          <PrivateRouter path={AppRoutes.home}>
            <Home />
          </PrivateRouter>
        } />

        {/* Login Route */}
        <Route path={AppRoutes.login} element={<Login />} />

        {/* PropertyDashboard Route */}
        <Route path={AppRoutes.dashboard} element={<PropertyDashboard />} />

        {/* Collabo Card Route */}
        <Route path={AppRoutes.collabo} element={<Collabo />} />

        {/* Property Creation Route */}
        <Route path={AppRoutes.propertyCreation} element={
          <PrivateRouter path={AppRoutes.propertyCreation}>
            <PropertyCreation />
        </PrivateRouter>
        }
        />
        {/* <Route path={AppRoutes.propertyCreation} element={<PropertyCreation />} /> */}

        {/* properties Dashboard*/}
        <Route path={AppRoutes.propertiesDash} element={<PropertiesDash />} />

        {/* Property Creation Mob Route */}
        <Route path={AppRoutes.propertyCreationMob} element={<PropertyCreationMob />} />

        {/* properties Dashboard Mob*/}
        <Route path={AppRoutes.propertiesDashMob} element={<PropertiesDashMob />} />

        {/* properties View*/}
        <Route path={AppRoutes.propertyView} element={<PropertyView />} />

        {/* properties View Mob*/}
        <Route path={AppRoutes.propertyViewMob} element={<PropertyViewMob />} />

        {/* personalForm */}
        <Route path={AppRoutes.personalForm} element={<PersonalForm />} />

        {/* personalFormTable */}
        <Route path={AppRoutes.personalFormTable} element={<PersonalFormTable />} />

        {/* propertyTable */}
        <Route path={AppRoutes.propertyTable} element={<PropertyTable />} />

        {/* For unknow/non-defined path */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouterApp;
