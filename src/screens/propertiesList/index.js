import React from "react";
import { withNavBars } from "./../../HOCs";
import { PropertiesDash } from "./propertiesDash";

class PropertiesDashParent extends React.Component {
  render() {
    return <PropertiesDash />;
  }
}

export default withNavBars(PropertiesDashParent);
