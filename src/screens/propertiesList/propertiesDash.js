import React, { useState } from 'react';
import { Button, Divider, Grid, Stack, Typography,Fab } from '@mui/material';
import { BigCards, CustomizeTable, DataTabs, InputField, RequestCard, SmallCards } from '../../components';
import { PropertiesDashStyles } from './style';
import { Box } from '@mui/system';
import { AppRoutes } from '../../router/routes';
import { useNavigate } from 'react-router-dom';
import { AddPropIcon } from '../../assets/addPropIcon';
import { PropertyFinderType, Propertyheading, Propertypath, Propertyrow } from "../../utils"
import Charts from '../../components/chartComp';
import AddIcon from '@mui/icons-material/Add';






export const PropertiesDash = props => {
    const navigate = useNavigate();
    const [activeTab, setActiveTab] = useState(1);
    const [inputField, SetInputField] = useState("")

    const classes = PropertiesDashStyles();


    const data = [
        {
            id: 0,
            number: 14,
            title: "Active Properties",
            image: "images/ActiveProperties.svg"
        },
        {
            id: 1,
            number: 20,
            title: "Blocks",
            image: "images/Leads.svg"
        },
        {
            id: 3,
            number: 18,
            title: "Floors",
            image: "images/Floors.svg"
        },
        {
            id: 4,
            number: 10,
            title: "Residents",
            image: "images/Residents.png"
        },
        {
            id: 5,
            number: 12,
            title: "Active Unit",
            image: "images/Active Unit.png"
        },
        {
            id: 6,
            number: 7,
            title: "Vaccant",
            image: "images/VacantCard.png"
        },
        {
            id: 7,
            number: 6,
            title: "Reserved",
            image: "images/Reserved.svg"
        },
        {
            id: 8,
            number: 20,
            title: "Occupied",
            image: "images/Occupied.png"
        },
    ]

    const datas = [
        {
            title:"Property Types",
            expandIcon:"/images/icons8-enlarge-48.png",
            type:"pie",
            series: [44, 55, 13, 33],
            options: {
              chart: {
                width: "100%",
                height:350,
                type: 'pie',
              },
              dataLabels: {
                enabled: false
              },
              labels: ['Vacant', 'Occupied', 'Reserved', 'Listed',],
              responsive: [
                {
                    breakpoint: 2000,
                    options: {
                        chart: {
                            // width: 350,
                            height:350,
                            type: "pie"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                    breakpoint: 1000,
                    options: {
                        chart: {
                            // width: 350,
                            height:350,
                            type: "pie"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                    breakpoint: 600,
                    options: {
                        chart: {
                            // width: 350,
                            // height:350,
                            type: "pie"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                breakpoint: 480,
                options: {
                  chart: {
                    // width: 350,
                    // height:350
                  },
                  legend: {
                    position: "bottom"
                  }
                },
                legend: {
                  position: "bottom",
                },
                }
                
            ],
         
            },

        },
        
        {   
            title:"Unit Category",
            expandIcon:"/images/icons8-enlarge-48.png",
            type: 'bar',
            height: 300,
            width:300,
          
              series: [{
                name: 'PRODUCT A',
                data: [44, 55, 41, 67, 22]
              }, {
                name: 'PRODUCT B',
                data: [13, 23, 20, 8, 13]
              }, {
                name: 'PRODUCT C',
                data: [11, 17, 15, 15, 21]
              }, {
                name: 'PRODUCT D',
                data: [21, 7, 25, 13, 22]
              }],
              options: {
                chart: {
                  type: 'bar',
                  height: 350,
                  stacked: true,
                  toolbar: {
                    show: false
                  },
                  zoom: {
                    enabled: false
                  }
                },
                responsive: [{
                  breakpoint: 480,
                  options: {
                    // legend: {
                    //   position: 'bottom',
                    //   offsetX: -10,
                    //   offsetY: 0
                    // }
                  }
                }],
                plotOptions: {
                  bar: {
                    horizontal: false,
                    // borderRadius: 10
                  },
                },
                xaxis: {
                  type: 'datetime',
                  categories: ['01/01/2011 GMT', '01/02/2011 GMT', '01/03/2011 GMT', '01/04/2011 GMT',
                    '01/05/2011 GMT', 
                  ],
                },
                legend: {
                  position: 'bottom',
                  offsetY: 40,
                  
                },
                fill: {
                  opacity: 1
                }
              },
            
            
            

          
          
        },

        {
            title:"Vacant Units By Property",
            expandIcon:"/images/icons8-enlarge-48.png",
            type: "bar",
            height: 300,
            series: [{
                data: [400, 430, 448, 470, 540,]
            }],
            options: {
                chart: {
                    type: 'bar',
                    height: 350
                },
                plotOptions: {
                    bar: {
                        borderRadius: 4,
                        horizontal: true,
                    }
                },
                dataLabels: {
                    enabled: true
                },
                legend: {
                  show: true
                },
                xaxis: {
                    categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy'],
                }
            }
        },

        {
            title:"Property Types",
            expandIcon:"/images/icons8-enlarge-48.png",
            type: 'donut',
            height: 350,
            series: [44, 55],
            options: {
              chart: {
                width: "100%",
                height:"100%",
                type: 'donut',
              },
              dataLabels: {
                enabled: false
              },
              labels: ['Residential', 'Commercial'],
              responsive: [
                {
                    breakpoint: 2000,
                    options: {
                        chart: {
                            // width: 350,
                            // height:350,
                            type: "donut"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                    breakpoint: 1000,
                    options: {
                        chart: {
                            // width: 350,
                            // height:350,
                            type: "donut"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                    breakpoint: 600,
                    options: {
                        chart: {
                            // width: 350,
                            // height:350,
                            type: "donut"
                          },
                      legend: {
                        position: "bottom"
                      }
                    }
                  },
                {
                breakpoint: 480,
                options: {
                  chart: {
                    // width: 350,
                    // height:350,
                  },
                  legend: {
                    position: "bottom"
                  }
                }
                }
              ],
              legend: {
                position: 'bottom',
                offsetY: 5,
                // height: 30,
              }
            },
          
          
        }        
        
    ]


    const handleEvent = (event) => {
        SetInputField(event.target.value);
    }

    const createIcon = () => {
        navigate(AppRoutes.propertyCreation);
    }


    const tabsData = [
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "10 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Drinage Repair",
            requestId: "K-F01-U277",
            date: "20 NOV 22",
        },
        {
            requestName: "Maintenance",
            title: "Plumbing Repair",
            requestId: "K-F01-U277",
            date: "30 OCT 22",
        },
    ]

    const tabs = [
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Gendral</Typography>,
            value: 1,
            body: <div style={{ padding: "0px" }}>

                <Box className={classes.reqHeadSection}>
                    <Typography className={classes.reqHead}>General Request</Typography>
                    <Button>View all</Button>
                </Box>

                <Box className={classes.fieldSection}>
                    <InputField handleChange={handleEvent} value={inputField} />
                </Box>


                <Box className={classes.tabsDatasSection}>
                    {tabsData?.map?.((item) => {
                        return (
                            <Box marginBottom={"8px"}>
                                <RequestCard data={item} />
                            </Box>


                        )
                    })}
                </Box>
            </div>
        },
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Maintenance</Typography>,
            value: 2,

            body: <div style={{ padding: "0px" }}>

                <Box className={classes.reqHeadSection}>
                    <Typography className={classes.reqHead}>Maintaince Request</Typography>
                    <Button>View all</Button>
                </Box>

                <Box className={classes.fieldSection}>
                    <InputField handleChange={handleEvent} value={inputField} />
                </Box>

                <Box className={classes.tabsDatasSection}>
                    {tabsData?.map?.((item) => {
                        return (
                            <Box marginBottom={"8px"}>
                                <RequestCard data={item} />
                            </Box>
                        )
                    })}
                </Box>

            </div>
        },
    ]









    return (
        <div className={classes.root}>



            {/* top section */}
            <Grid container spacing={2} marginBottom={"8px"}>
                {data?.map((item) => {
                    return (
                        <Grid item lg={1.5} md={4} sm={4} xs={12}>
                            <SmallCards data={item} />
                        </Grid>
                    )
                })}
            </Grid>

            {/* second section */}
            <Grid container spacing={2} marginTop={"8px"}>
                {
                    datas?.map?.((item) => {
                        return (
                            <Grid item lg={3} md={6} sm={6} xs={12}>
                                <Box className={classes.bigCards}>
                                    <Box className={classes.cardHead}>
                                        <Typography className={classes.headTitle}>{item?.title}</Typography>
                                        <img src={item?.expandIcon}/>
                                    </Box>
                                    <Charts data={item} />
                                </Box>
                            </Grid>
                        )
                    })

                }

             

            </Grid>


            <Grid container spacing={2} marginTop={"8px"} >
                <Grid item lg={6} md={6} sm={12} xs={12} >

                    <Box className={classes.tabBox}>

                        <Box className={classes.requestSection}>
                            <Stack direction={"row"} alignItems={"center"} className={classes.parentRequest} divider={
                                <Divider orientation="vertical" flexItem className={classes.borderRightStyling} />} spacing={2} >
                                <Box mr={2}>
                                    <Typography className={classes.generalReq} pb={0.5}>General Requests</Typography>
                                    <Typography className={classes.generalNumber}>12</Typography>
                                </Box>
                                <Box>
                                    <Typography className={classes.generalReq} pb={0.5}>Maintaince</Typography>
                                    <Typography className={classes.generalNumber}>15</Typography>
                                </Box>
                            </Stack>
                        </Box>

                        <DataTabs tabs={tabs} activeTab={activeTab} handleChangeTab={(val) => setActiveTab(val)} />
                    </Box>
                </Grid>
                <Grid item lg={6} md={6} sm={12} xs={12} >
                    {/* <Box className={classes.tableBox}> */}

                    <CustomizeTable
                        heading={Propertyheading}
                        rows={Propertyrow}
                        path={Propertypath}
                        dataType={PropertyFinderType}
                        height={420}
                    // showpagination={true}
                    // count="2"
                    // showpdfbtn={false}
                    // showexcelbtn={false}
                    // showSearch={false}
                    // handleIcon={handleIcon}
                    // onClick={(data) => console.log("")}
                    // onUnitClick={onUnitClick}
                    // tableType="no-side"
                    // view={true}
                    // handlePagination={handlePagination}
                    // handleChangeLimit={handleChangeLimit}
                    // totalRowsCount={propertyList?.totalRowsCount}
                    // page={page}
                    // limit={limit}
                    // height={'calc(100vh - 290px)'}

                    />
                    {/* </Box> */}
                </Grid>

            </Grid>

            <Fab className={classes.AddIcon} onClick={createIcon}>
                <AddIcon   />
      
            </Fab>


        </div>
    )
}