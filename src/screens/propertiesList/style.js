import { makeStyles } from "@mui/styles";


export const PropertiesDashStyles = makeStyles((theme) => ({
    root:{
        // backgroundColor:"#F5F7FA ",
        padding:"16px",
        height:"100vh",
        
        
    },
    tabBox:{
        backgroundColor: "white",
        borderRadius: "4px",
        height: "100%",
        maxHeight:"420px",
        boxShadow:"0px 3px 30px #5C86CB2E",

    },
    tableBox:{
        backgroundColor: "white",
        borderRadius: "4px",
        height: "100%",
        maxHeight:"420px",
        overflow:"auto",
        boxShadow:"0px 3px 30px #5C86CB2E",

    },
    reqHead:{
        backgroundColor:"red",
        width:"50%"
    },
    AddIcon:{
        // color:"#0065E6",
        fontSize:"60px",
        position:"fixed",
        bottom:"20px",
        right:"10px",
        cursor:"pointer",
        zIndex:"300",
        backgroundColor:"#0065E6",
        color:"white",
        "&:hover":{
            backgroundColor:"#0065E6",
            color:"white",
        }

    },
    headerCreate: {
        boxShadow: "0px 10px 25px #0000000A",
        background: "#FFFFFF 0% 0% no-repeat padding-box",
        padding: "8px 22px",
    },
   arrowColor: {
        color: "#091B29",
        backgroundColor: "#E4E8EE",
        width: "24px",
        height: "24px",
        borderRadius: "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },

    headerText: {
        color: "#091B29",
        fontSize: "16px",
        fontWeight: "bold",
    },
    generalReq: {
        color: "#091B29",
        fontSize: "14px",
        fontWeight: "bold"
    },
    generalNumber: {
        color: "#091B29",
        fontSize: "20px",
        fontWeight: "bold"
    },
    borderRightStyling: {
        height: "21px",
        margin: "auto 0 auto 0 !important"
    },
    parentRequest: {
        borderRadius: "4px",
        backgroundColor: "#F5F7FA",
        padding: "14px",
        display: "inline-flex",
        // width:"45%"
    },
    reqHead:{
        color:"#4E5A6B",
        fontSize:"14px",
    },
    requestSection:{
        padding:"12px 12px 2px"
    },
    reqHeadSection:{
        display:"flex",
        alignItems:"center" ,
        justifyContent:"space-between",
        marginBottom:"8px",       
    },
    fieldSection:{
        marginBottom:"8px"
    },
    tabsDatasSection:{
        overflow:"auto",
        height:"100%",
        maxHeight:"160px",
        "&::-webkit-scrollbar": {
            display:"none"
            },
    },
    bigCards:{
        backgroundColor: "white",
        borderRadius: "4px",
        padding: "12px",
        height:"100% !important",
        boxShadow:"0px 3px 30px #5C86CB2E",
        minHeight:"400px"
    },
    cardHead:{
        display:"flex",
        justifyContent:"space-between",
        alignItems:"center", 
        marginBottom:"30px"     
    },
    headTitle:{
        color:"#091B29",
        fontSize:"16px",
        fontWeight:"600",
        fontFamily:"sans-serif",
    },
    
    
    

}));