import React, { useState } from 'react';
import { Avatar, Box, Divider, Grid, Typography, Container } from '@mui/material';
import { PropertyViewMobStyles } from './style';
import { RequestCard } from '../../components';









export const PropertyViewMob = props => {
    
    const classes = PropertyViewMobStyles();


    const data = {
        requestName:"Maintenance",
        title:"Water Lekage Repair",
        requestId:"K-F01-U277",
        date:"22 JAN 21",
        
    }


    




    return (

        <div className={classes.root}>

            <Container maxWidth="sm" sx={{padding:"0px"}}>
            
            <Grid container padding={"16px"} className={classes.topSection} alignItems={"center"}>
                <Grid item lg={12} md={12} sm={12} xs={12}  marginBottom={"20px"}>
                    <Box className={classes.avatarHead}>
                    <Avatar className={classes.leftSectionAvatar} src='/images/NoPath.png'></Avatar>
                    </Box>
                    

                </Grid>
                <Divider orientation="vertical" flexItem />

                <Grid item lg={12} md={12} sm={12} xs={12}>
                    <Typography className={classes.labelTitle}>
                        PROPERTY DETAILS
                    </Typography>

                    <Grid container>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Company Name</Typography>
                            <Typography className={classes.text}>Crayond</Typography>

                        </Grid>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Property Name</Typography>
                            <Typography className={classes.text}>Rubix Appartment</Typography>

                        </Grid>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Property Type</Typography>
                            <Typography className={classes.text}>Property Type</Typography>

                        </Grid>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Property Purpose</Typography>
                            <Typography className={classes.text}>Residential</Typography>

                        </Grid>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Payment Period</Typography>
                            <Typography className={classes.text}>Monthly</Typography>

                        </Grid>
                        <Grid item lg={6} md={6} sm={6} xs={6}>
                            <Typography className={classes.labelText}>Status</Typography>
                            <Typography className={classes.text}>Active</Typography>

                        </Grid>
                    </Grid>

                    <Grid container marginTop={"24px"}>
                        <Typography className={classes.labelText}>Property Description</Typography>
                        <Typography className={classes.text}>A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, a street bike available at a starting price of Rs. 1,62,916 in India. It is available in 3 variants and 8 colours with top variant price starting from The Yamaha</Typography>

                    </Grid>
                </Grid>
            </Grid>

            <Grid container padding={"16px"} className={classes.topSection} >
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Revenue Type</Typography>
                    <Typography className={classes.text}>Lease</Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Carpet Area</Typography>
                    <Typography className={classes.text}>10000<span className={classes.sqft}>Sq. Ft</span></Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Total Area</Typography>
                    <Typography className={classes.text}>165480<span className={classes.sqft}>Sq. Ft</span></Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Year Built</Typography>
                    <Typography className={classes.text}>22-02-2020</Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Handover Date</Typography>
                    <Typography className={classes.text}>22-02-2022</Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Public Listing</Typography>
                    <Typography className={classes.text}>Public</Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={6}>
                    <Typography className={classes.labelText}>Pets Allowed</Typography>
                    <Typography className={classes.text}>Yes</Typography>
                </Grid>

            </Grid>

            <Grid container padding={"16px"} className={classes.topSection}>
                <Grid item lg={12} md={12} sm={12} xs={12} marginBottom={"10px"}>
                    {/* <Box className={classes.addressCard}>
                        <Typography className={classes.addressTitle}><span><img src='/images/location Icon.svg' /></span>ADDRESS</Typography>
                        <Typography className={classes.addressText}>23 Main Street, 3rd Cross street, 3rd Sector, Chennai, Tamilnadu, India -60001</Typography>
                        <Typography className={classes.latText}><span className={classes.latTitle}>Latitude:</span> 27.2046°N</Typography>
                        <Typography className={classes.latText}><span className={classes.latTitle}>Longitude:</span> 77.4977°E</Typography>
                    </Box> */}
                    <Box className={classes.mapSection}>
                        <img className={classes.mapSectionImg} src='/images/googleMapsTask.png' />
                    </Box>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12} marginBottom={"12px"} >
                    <Box className={classes.addressCard}>
                        <Typography className={classes.addressTitle}><span><img src='/images/location Icon.svg' /></span>ADDRESS</Typography>
                        <Typography className={classes.addressText}>23 Main Street, 3rd Cross street, 3rd Sector, Chennai, Tamilnadu, India -60001</Typography>
                        <Typography className={classes.latText}><span className={classes.latTitle}>Latitude:</span> 27.2046°N</Typography>
                        <Typography className={classes.latText}><span className={classes.latTitle}>Longitude:</span> 77.4977°E</Typography>
                    </Box>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12} marginBottom={"12px"}>
                    <Box className={classes.addressCard}>
                        <Typography className={classes.addressTitle}><span><img src='/images/phoneIcon.svg' /></span>CONTACT & OTHER INFORMATION</Typography>
                        <Box display={"flex"} alignItems={"center"} marginTop={"16px"}>
                            <Box textAlign={"left"} width={"50%"} >
                                <Typography className={classes.latTitle}>Business Phone :</Typography>
                                <Typography className={classes.addressText}>044 23224944</Typography>
                            </Box>
                            <Box textAlign={"left"} width={"50%"}>
                                <Typography className={classes.latTitle}>Mobile Phone :</Typography>
                                <Typography className={classes.addressText}>044 23224944</Typography>
                            </Box>
                        </Box>


                        <Box display={"flex"} alignItems={"center"} marginTop={"16px"}>
                            <Box textAlign={"left"} width={"50%"} >
                                <Typography className={classes.latTitle}>Website :</Typography>
                                <Typography className={classes.addressText}>propertyautomate.com</Typography>
                            </Box>
                            <Box textAlign={"left"} width={"50%"}>
                                <Typography className={classes.latTitle}>Email Address :</Typography>
                                <Typography className={classes.addressText}>mail@propertyautomate.com</Typography>
                            </Box>
                        </Box>
                    </Box>

                </Grid>
            </Grid>

            </Container>




            

        </div>


    )
}