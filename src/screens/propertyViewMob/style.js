import { makeStyles } from "@mui/styles";


export const PropertyViewMobStyles = makeStyles((theme) => ({
    root:{
        padding:"30px",
        backgroundColor:"#F5F7FA",
    },

    topSection:{
        backgroundColor:"white",
        
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px",
        
    },
    rightSection:{
        backgroundColor:"white",        
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        height:"264px",
        // marginLeft:"16px"
        
    },
    sqft:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
        marginLeft:"8px"
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
        paddingTop:"10px"
    },
    labelTitle:{
        color:"#4E5A6B",
        fontSize:"12px",
        paddingBottom:"16px",
        fontFamily:"sans-serif",
        fontWeight:"600"
    },
    text:{
        color:"#091B29",
        fontSize:"14px",
        // paddingBottom:"8px",
        fontFamily:"sans-serif",
        fontWeight:"500"
    },
    addressTitle:{
        color:"#091B29",
        fontSize:"12px",
        fontFamily:"sans-serif",
        fontWeight:"600",
        display:"flex",
        alignItems:"center"

    },
    addressText:{
        color:"#091B29",
        fontSize:"14px",
        fontFamily:"sans-serif",
        fontWeight:"500",
        paddingTop:"12px",
        // word-break: break-word;
        wordBreak:"break-word",
    },
    addressCard:{
        border:"1px solid #E4E8EE",
        borderRadius:"8px",
        height: "100%",
        padding: "16px",
        marginBottom:"12px"
    },
    latTitle:{
        color:"#98A0AC",
        fontSize:"12px",
        fontFamily:"sans-serif",
        // fontWeight:"500",


    },
    latText:{
        color:"#091B29",
        fontSize:"12px",
        fontFamily:"sans-serif",
        paddingTop:"16px"
        
    },
    leftSectionAvatar:{
        margin: "auto",
        width: "140px",
        height: "140px",              
    },
    mapSection:{        
        width:"100%"
    },
    mapSectionImg:{
        width:"100%",
        borderRadius:"8px",
    },
    avatarHead:{
        borderBottom:"1.4px solid #98A0AC",
        width:"50%",
        margin:"auto",
        paddingBottom:"14px",
    }
   
    
    

}));