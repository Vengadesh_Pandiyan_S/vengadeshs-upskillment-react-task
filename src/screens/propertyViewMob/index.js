import React from "react";
import { PropertyViewMob } from "./propertyViewMob";
import { withNavBarsPWA } from "./../../HOCs";

class PropertyViewMobParent extends React.Component {
  render() {
    return <PropertyViewMob />;
  }
}

export default withNavBarsPWA(PropertyViewMobParent);
                                