import React from "react";
import {PersonalFormTable} from "./personalFormTable"
import { withNavBars } from "./../../HOCs";

class PersonalFormTableParent extends React.Component {
  render() {
    return <PersonalFormTable />;
  }
}

export default withNavBars(PersonalFormTableParent);
