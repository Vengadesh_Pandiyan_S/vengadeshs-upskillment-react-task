import React, { useState, } from 'react';
import { Box, Button, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, Paper } from '@mui/material';
import { PersonalFormTableStyles } from './style';





const initial = {
    name: '',
    age: '',
    dateOfBirth: '',
    mail: '',
    number: '',
    error: {
        mail: '',
        age: '',
        number: '',
        dateOfBirth: '',
        name: '',

    }
}

export const PersonalFormTable = props => {
    const classes = PersonalFormTableStyles();

    const [data, setData] = useState({ ...initial });

    // updateState
    const updateState = (key, value) => {
        let error = data.error
        error[key] = ""
        setData({ ...data, [key]: value })
    }

    // submit
    const submit = () => {
        // if(!validate){
        //     return(
        //         // setData(errorMsg)
        //     )
        // }
        console.log(data);


    }

    // clear
    const clear = () => {
        setData(initial);
    }

    // form validation
    const validate = () => {
        let isValid = true;
        let error = data.error

        if (setData < 0) {


        }
    }

    const rows = [
        {
            name:"vengadesh",
            age:"25",
            number:"8778210733",
            dateOfBirth:"30-10-1996",
            mail:"vengadesh@crayond.co",
        },
        {
            name:"Arun",
            age:"23",
            number:"9715624578",
            dateOfBirth:"08-09-1998",
            mail:"arun@crayond.co",
        },
    ]







    return (
        <>
            <div className={classes.root}>
                <Typography className={classes.labelTitle}>Personal Information</Typography>
                {/* top section */}
                <Grid container>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell >Name</TableCell>
                                    <TableCell >Age</TableCell>
                                    <TableCell >Mail -ID</TableCell>
                                    <TableCell >Mobile Number</TableCell>
                                    <TableCell >Date of Birth</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row) => (
                                    <TableRow
                                        key={row.name}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell >{row.age}</TableCell>
                                        <TableCell >{row.mail}</TableCell>
                                        <TableCell >{row.number}</TableCell>
                                        <TableCell >{row.dateOfBirth}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>


                {/* bottom section */}
                {/* <Box className={classes.bottomSection}>
                    <Button onClick={clear} className={classes.cancelBtn} variant='outlined' >Cancel</Button>
                    <Button onClick={submit} className={classes.createBtn} variant='contained'>Create</Button>
                </Box> */}
            </div>
        </>
    )
}