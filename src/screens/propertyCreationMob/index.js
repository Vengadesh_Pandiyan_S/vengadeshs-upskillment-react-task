import React from "react";
import { PropertyCreationMob } from "./propertyCreationMob";
import { withNavBarsPWA } from "./../../HOCs";

class PropertyCreationMobParent extends React.Component {
  render() {
    return <PropertyCreationMob />;
  }
}

export default withNavBarsPWA(PropertyCreationMobParent);



