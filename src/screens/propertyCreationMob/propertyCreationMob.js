import React, { useState } from 'react';
import { Avatar, Box, Button, Container, Grid, Typography } from '@mui/material';
import { BigCards, DataTabs, InputField, PasswordField, RequestCard, SelectField, Tabs } from '../../components';
import { PropertyCreationMobStyles } from './style';








export const PropertyCreationMob = props => {
    const [selectField, SetselectField] = useState("")
    const [inputField, SetInputField] = useState("")
    const [activeTab, setActiveTab] = React.useState(1);
    const classes = PropertyCreationMobStyles();


    const handleChange = (event) => {
        SetselectField(event.target.value);
    }
    const handleEvent = (event) => {
        SetInputField(event.target.value);
    }
    const options = [
        {
            label: "one",
            value: "one",

        },
        {
            label: "two",
            value: "two",

        },
        {
            label: "three",
            value: "three",
        },
    ]

    const tabsData = [
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
    ]

    const tabs = [
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Gendral</Typography>,
            value: 1,
            body: <div>

                {tabsData?.map?.((item) => {
                    return (
                        <RequestCard data={item} />
                    )
                })}
            </div>
        },
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Maintenance</Typography>,
            value: 2,
            body: <div>
                {tabsData?.map?.((item) => {
                    return (
                        <RequestCard data={item} />
                    )
                })}
            </div>
        },
    ]




    return (
        <>
            <div className={classes.root}>

            <Container maxWidth="sm">
                {/* Top section */}
                <Grid container spacing={2}>
                    <Grid item lg={12} md={12} sm={12} xs={12} className={classes.leftSection}>

                        {/* <Box > */}
                            <Typography className={classes.labelTitle} textAlign={"center"}>PROPERTY IMAGE</Typography>
                            <Avatar className={classes.leftSectionAvatar} src='./images/cityImage.svg'></Avatar>
                            <Button className={classes.leftSectionBtn} variant="outlined">Upload Image</Button>
                        {/* </Box> */}

                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12} className={classes.rightSection}>
                        {/* <Box > */}
                            <Typography className={classes.labelTitle}>
                                PROPERTY DETAILS
                            </Typography>

                            <Grid container spacing={2}>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Company Name</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Property Name</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Payment Period</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Status</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>
                            </Grid>
                        {/* </Box> */}
                    </Grid>

                </Grid>

                {/* second section */}
                <Grid container spacing={2} className={classes.middleSection}>
                
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Property Type</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Property Purpose</Typography>
                        <SelectField handleChange={handleChange} value={selectField} options={options} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Revenue Type</Typography>
                        <SelectField handleChange={handleChange} value={selectField} options={options} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Measurement Unit</Typography>
                        <SelectField handleChange={handleChange} value={selectField} options={options} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Carpet Area</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Total Area</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Year Built</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Handover Date</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Typography className={classes.labelText}>Public Listing</Typography>
                        <InputField handleChange={handleEvent} value={inputField} />
                    </Grid>

                </Grid>

                {/* third Section */}
                <Grid container spacing={2} className={classes.thirdSection}>
                    <Typography className={classes.labelTitle}>ADDRESS</Typography>
                    <Grid container spacing={2}>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Box className={classes.mapSection}>
                        <img className={classes.mapSectionImg} src='/images/googleMapsTask.png' />
                        </Box>
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <Grid container spacing={2}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Door Number</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Address Line 1</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Address Line 2</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>

                            </Grid>

                            <Grid container spacing={2} marginTop={"10px"}>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Landmark</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Area</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Area</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>State</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>

                            </Grid>

                            <Grid container spacing={2} marginTop={"10px"}>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Country</Typography>
                                    <SelectField handleChange={handleChange} value={selectField} options={options} />
                                </Grid>

                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <Typography className={classes.labelText}>Pincode</Typography>
                                    <InputField handleChange={handleEvent} value={inputField} />
                                </Grid>



                            </Grid>


                        </Grid>

                    </Grid>
                </Grid>

                {/* fourth section */}
                <Grid container spacing={2} className={classes.fourthSection}>
                    <Typography className={classes.labelTitle}>CONTACT & OTHER INFORMATION</Typography>
                    <Grid container spacing={2}>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <Typography className={classes.labelText}>Business Phone</Typography>
                            <SelectField handleChange={handleChange} value={selectField} options={options} />
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <Typography className={classes.labelText}>Mobile Phone</Typography>
                            <SelectField handleChange={handleChange} value={selectField} options={options} />
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <Typography className={classes.labelText}>Website</Typography>
                            <SelectField handleChange={handleChange} value={selectField} options={options} />
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <Typography className={classes.labelText}>Email Address</Typography>
                            <SelectField handleChange={handleChange} value={selectField} options={options} />
                        </Grid>

                    </Grid>
                </Grid>

                <Grid container>
                    <Box className={classes.bottomSection}>
                    <Button className={classes.cancelBtn} variant='outlined' >Cancel</Button>
                    <Button  className={classes.createBtn} variant='contained'>Create</Button>
                </Box>
                </Grid>
            </Container>


            </div>
            
        </>
    )
}