import { makeStyles } from "@mui/styles";


export const PropertyCreationMobStyles = makeStyles((theme) => ({
    root:{
        // backgroundColor:"#F5F7FA ",
        padding:"30px",
        backgroundColor:"#F5F7FA",
        overflow:"auto",
        height:"100vh",
    },
    leftSection:{
        backgroundColor:"white",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginBottom:"10px",
        
    },
    bottomSection:{
        backgroundColor:"white",
        padding:"12px",
        position:"fixed",
        display:"flex",
        justifyContent:"center",
        width:"100%",
        bottom:0,
        left:0,
        right:0
        
    },
    cancelBtn:{
        marginRight:"10px",
        
    },
    rightSection:{
        backgroundColor:"white",        
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        height:"auto",
        // marginLeft:"16px"
        
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },
    labelTitle:{
        color:"#4E5A6B",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
        fontWeight:"600"
    },
    middleSection:{
        backgroundColor:"#FFFFFF",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px"
    },
    thirdSection:{
        backgroundColor:"#FFFFFF",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px",
        
    },
    fourthSection:{
        backgroundColor:"#FFFFFF",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px",
        marginBottom:"40px"
    },
    leftSectionAvatar:{
        margin: "auto",
        width: "140px",
        height: "140px",
        marginTop: "12px",
        backgroundColor:"#F2F4F7",
        borderRadius:"71px"
    },
    leftSectionBtn:{
        margin:"auto",
        display:"flex",
        marginTop: "12px",
    },
    bottomBtns:{
        backgroundColor:"white",
        marginTop:"10px",
        position:"fixed",
         bottom:"0"

    },
    mapSection:{        
        width:"100%",
        height:"auto"
    },
    mapSectionImg:{
        width:"100%",
        borderRadius:"8px",
        height: "243px",

    }
    

    
    

}));