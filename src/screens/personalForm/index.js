import React from "react";
import { PersonalForm } from "./form";
import { withNavBars, withAllContexts } from "./../../HOCs";
// import {} from "../"

class PersonalFormParent extends React.Component {
  render() {
    return <PersonalForm {...this.props}/>;
  }
}

export default withAllContexts(withNavBars(PersonalFormParent));
