import React, { useState, useEffect } from 'react';
import { Box, Button, Grid, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import { InputField } from '../../components';
import { PersonalFormStyles } from './style';
import { Edit } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { AlertProps } from '../../utils';





const initial = {
    name: '',
    age: '',
    dateOfBirth: '',
    mail: '',
    number: '',
    error: {
        mail: '',
        age: '',
        number: '',
        dateOfBirth: '',
        name: '',

    }
}

export const PersonalForm = props => {
    // debugge
    const classes = PersonalFormStyles();

    const [data, setData] = useState({ ...initial });
    const [dataArr, setDataArr] = useState([]);
    const [index, setIndex] = useState(null);
    const [getAllData, setgetAllData] = useState([]);
    


    const updateState = (key, value) => {
        let error = data.error
        error[key] = ""
        setData({ ...data, [key]: value, error });
    }

    const tableData = (value) => {
        setDataArr([
            ...dataArr,
            value
        ])
        // debugger
    }

    // submit function
    const submit = () => {
        debugger
        if (validate()) {

            const form = {

                name: data?.name,
                age: data?.age,
                dateOfBirth: data?.dateOfBirth,
                mail: data?.mail,
                // star:true,
                number: data?.number,
            };
            if(index>=0 && index!== null){
                const editData = getAllData[index]
                axios.put(`https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users/${editData?.id}`, 
                form
            ).then(function (response) {
                // console.log(response.data);
                // history('/', { replace: true });
                console.log(response?.data)
                getData()
                setIndex(null)
                setData({
                    ...initial
                })
                props.alert.setSnack({
                    open: true,
                    severity: AlertProps.severity.success,
                    msg: "Person Updated successfully",
                    vertical: AlertProps.vertical.top,
                    horizontal: AlertProps.horizontal.center,
                  });
            });

            }
            else{
                
                axios.post('https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users', 
                form
            ).then(function (response) {
                // console.log(response.data);
                // history('/', { replace: true });
                console.log(response?.data)
                getData()
                props.alert.setSnack({
                    open: true,
                    severity: AlertProps.severity.success,
                    msg: "Person Added successfully",
                    vertical: AlertProps.vertical.top,
                    horizontal: AlertProps.horizontal.center,
                  });
            })

            }
           

            // if (index > -1) {
            //     dataArr[index] = form;
            //     setDataArr(dataArr);
            // }
            // else {
            //     tableData(form);
            // }

            console.log(dataArr, "setData");


            // alert.setSnack({
            //     ...alert,
            //     open: true,
            //     severity: AlertProps.severity.success,
            //     msg: "Added Successfully",
            // });


            setData({
                name: "",
                age: "",
                dateOfBirth: '',
                mail: "",
                number: "",
                error: {
                    name: "",
                    age: "",
                    dateOfBirth: '',
                    mail: "",
                    number: ""
                }
            })
        }

        // postData()



    }

    // editicon function
    const editIcon = (row, index) => {
        // debugger
        // console.log("edits", dataArr[index])
        // const getEditData = getAllData[index]
        setIndex(index);
        // setIndex({...dataArr[index]});
        setData({
            ...data,
            ...row,
            // name: dataArr[index]?.name,
            // age: dataArr[index]?.age,
            // number: dataArr[index]?.number,
            // dateOfBirth: dataArr[index]?.dateOfBirth,
            // mail: dataArr[index]?.mail,
        })

        // axios.delete(`https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users/${row?.id}`)
        // .then(function (response) {
        //     // console.log(response, "getData")
        //     // setgetAllData(response?.data)
        //     getData();

        // })


    }

    // delete columns in Table
    const deleteTableRows = (row, index) => {
        // const rows = [...dataArr];
        // rows.splice(index, 1);
        // setDataArr(rows);
        axios.delete(`https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users/${row?.id}`)
        .then(function (response) {
            // console.log(response, "getData")
            // setgetAllData(response?.data)
            getData();
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.success,
                msg: "Person Deleted successfully",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });

        })
    }

    // clearing form
    const clear = () => {
        setData({ ...initial });
    }

    // Form validation
    const validate = () => {
        let isValid = true;
        let error = data.error

        if (data?.name.length === 0) {
            isValid = false
            error.name = "Name is required"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.age.length === 0) {
            isValid = false
            error.age = "Age is required"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.mail.length === 0) {
            isValid = false
            error.mail = "Mail- ID is required"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.dateOfBirth.length === 0) {
            isValid = false
            error.dateOfBirth = "Date of Birth is required"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.number.length === 0) {
            isValid = false
            error.number = "Mobile Number is required"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.mail.length > 0 && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data?.mail)) {
            isValid = false
            error.mail = "Please Fill Valid Mail ID"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.age.length > 0 && !/^[0-9\b]+$/.test(data?.age)) {
            isValid = false
            error.age = "Please Fill Valid Age"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        if (data?.number.length > 0 && !/^(\+\d{1,3}[- ]?)?\d{10}$/.test(data?.number)) {
            isValid = false
            error.number = "Please Fill Valid Number"
            props.alert.setSnack({
                open: true,
                severity: AlertProps.severity.error,
                msg: "Invalid Data",
                vertical: AlertProps.vertical.top,
                horizontal: AlertProps.horizontal.center,
              });
        }
        setData({ ...data, error })
        return isValid
    }

    // get method data
    const getData = () => {        
        axios.get('https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users')
        .then(function (response) {
            // console.log(response, "getData")
            setgetAllData(response?.data)

        })
        

    }


    useEffect(() => {
      getData()
    
      
    }, [])
    





    return (
        <>
            <div className={classes.root}>
                <Typography className={classes.labelTitle}>Personal Information</Typography>
                <Grid container spacing={1} className={classes.fieldBackGrnd}>


                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        {/* <InputField onChange={(e) => handleChange('propertyType', e.target.value)} value={state?.propertyType ?? ''} /> */}
                        <Typography className={classes.labelText}>Name <span className={classes.redStar}>*</span></Typography>
                        <InputField value={data?.name} onChange={(e) => updateState("name", e.target.value)}
                            error={data.error.name.length > 0}
                            errorMsg={data.error.name}
                        />



                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Age <span className={classes.redStar}>*</span></Typography>
                        <InputField value={data?.age}
                            onChange={(e) => updateState("age", e.target.value)}
                            error={data.error.age.length > 0}
                            errorMsg={data.error.age}
                        />

                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Date of Birth <span className={classes.redStar}>*</span></Typography>
                        <InputField value={data?.dateOfBirth} onChange={(e) => updateState("dateOfBirth", e.target.value)}
                            error={data.error.dateOfBirth.length > 0}
                            errorMsg={data.error.dateOfBirth}
                        />

                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Mail - ID <span className={classes.redStar}>*</span></Typography>
                        <InputField value={data?.mail} onChange={(e) => updateState("mail", e.target.value)}
                            error={data.error.mail.length > 0}
                            errorMsg={data.error.mail}
                        />

                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >

                        <Typography className={classes.labelText}>Mobile Number <span className={classes.redStar}>*</span></Typography>
                        <InputField value={data?.number} onChange={(e) => updateState("number", e.target.value)}
                            error={data.error.number.length > 0}
                            errorMsg={data.error.number}
                        />

                    </Grid>

                </Grid>
                <Grid container spacing={1} className={classes.tableSection}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell >Name</TableCell>
                                    <TableCell >Age</TableCell>
                                    <TableCell >Mail -ID</TableCell>
                                    <TableCell >Mobile Number</TableCell>
                                    <TableCell >Date of Birth</TableCell>
                                    <TableCell >Edit</TableCell>
                                    <TableCell >Delete</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {getAllData.map((row, index) => (
                                    <TableRow
                                        key={row.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell >{row.age}</TableCell>
                                        <TableCell >{row.mail}</TableCell>
                                        <TableCell >{row.number}</TableCell>
                                        <TableCell >{row.dateOfBirth}</TableCell>
                                        <TableCell >
                                            <Edit onClick={() => editIcon(row, index)} />
                                        </TableCell>
                                        <TableCell >
                                            <DeleteIcon onClick={() => deleteTableRows(row, index)} />
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Box className={classes.bottomSection}>
                    <Button onClick={()=>clear()} className={classes.cancelBtn} variant='outlined' >Cancel</Button>
                    <Button onClick={()=>submit()} className={classes.createBtn} variant='contained'>{index>=0 && index!==null ? "Update":"Create"}</Button>
                </Box>
            </div>
        </>
    )
}