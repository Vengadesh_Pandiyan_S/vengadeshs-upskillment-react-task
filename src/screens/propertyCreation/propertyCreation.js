import React, { useState } from 'react';
import { Avatar, Box, Button, Checkbox, Grid, Stack, Typography } from '@mui/material';
import { DateShower, DownArrow, InputField, SelectField, } from '../../components';
import { PropertyCreationStyles } from './style';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { AppRoutes } from '../../router/routes';
import { useNavigate } from 'react-router-dom';
// import { Mapping } from '../../components/mapComponent'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import MapComponent from '../../components/mapComponent';




const initial = {
    propertyName: '',
    mail: '',
    handoverDate: new Date(),
    propertyType: '',   
    carpetArea: '',
    totalArea: '',
    builtArea: new Date(),
    companyName: '',
    paymentPeriod: '',
    status: '',
    propertyPurpose: '',
    revenueType: '',
    measurementUnit: '',
    doorNo: '',
    address1: '',
    address2: '',
    landmark: '',
    area: '',
    city: '',
    state: '',
    country: '',
    pincode: '',
    businessPhone: '',
    mobilePhone: '',
    website: '',

    error: {
        propertyName: '',
        mail: '',
        handoverDate: '',
        propertyType: '',
        carpetArea: '',
        totalArea: '',
        builtArea: '',
        companyName: '',
        paymentPeriod: '',
        status: '',
        propertyPurpose: '',
        revenueType: '',
        measurementUnit: '',
        doorNo: '',
        address1: '',
        address2: '',
        landmark: '',
        area: '',
        city: '',
        state: '',
        country: '',
        pincode: '',
        businessPhone: '',
        mobilePhone: '',
        website: '',

    }
}


export const PropertyCreation = props => {

    const classes = PropertyCreationStyles();
    const navigate = useNavigate();


    const [data, setData] = useState({ ...initial });
    const [value, setValue] = useState('')
    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };






    const updateState = (key, value) => {
        let error = data.error
        error[key] = ""
        setData({ ...data, [key]: value, error });
    }

    // submit function
    const submit = () => {


        if (validate()) {


            console.log(data, "setData");

            setData({
                propertyName: '',
                mail: '',
                handoverDate: '',
                propertyType: '',
                carpetArea: '',
                totalArea: '',
                builtArea: '',
                companyName: '',
                paymentPeriod: '',
                status: '',
                propertyPurpose: '',
                revenueType: '',
                measurementUnit: '',
                doorNo: '',
                address1: '',
                address2: '',
                landmark: '',
                area: '',
                city: '',
                state: '',
                country: '',
                pincode: '',
                businessPhone: '',
                mobilePhone: '',
                website: '',

                error: {
                    propertyName: '',
                    mail: '',
                    handoverDate: '',
                    propertyType: '',
                    carpetArea: '',
                    totalArea: '',
                    builtArea: '',
                    companyName: '',
                    paymentPeriod: '',
                    status: '',
                    propertyPurpose: '',
                    revenueType: '',
                    measurementUnit: '',
                    doorNo: '',
                    address1: '',
                    address2: '',
                    landmark: '',
                    area: '',
                    city: '',
                    state: '',
                    country: '',
                    pincode: '',
                    businessPhone: '',
                    mobilePhone: '',
                    website: '',

                }
            })
        }

    }

    const validate = () => {
        let isValid = true;
        let error = data.error


        if (data?.propertyName.length === 0) {
            isValid = false
            error.propertyName = "Property Name is required"

        }
        if (data?.handoverDate?.length === 0) {
            isValid = false
            error.handoverDate = "Handover Date is Required"
        }

        if (data?.companyName?.length === 0) {
            isValid = false
            error.companyName = "Company Name is Required"
        }

        if (data?.propertyType?.length === 0) {
            isValid = false
            error.propertyType = "Property Type is Required"
        }
        if (data?.carpetArea?.length === 0) {
            isValid = false
            error.carpetArea = "Carpet Area is Required"
        }
        if (data?.totalArea?.length === 0) {
            isValid = false
            error.totalArea = "Total Area is Required"
        }
        if (data?.builtArea?.length === 0) {
            isValid = false
            error.builtArea = "Built Area is Required"
        }
        if (data?.paymentPeriod?.length === 0) {
            isValid = false
            error.paymentPeriod = "Payment Period is Required"
        }
        if (data?.status?.length === 0) {
            isValid = false
            error.status = "Status is Required"
        }
        if (data?.propertyPurpose?.length === 0) {
            isValid = false
            error.propertyPurpose = "Property Purpose is Required"
        }
        if (data?.revenueType?.length === 0) {
            isValid = false
            error.revenueType = "Revenue Type is Required"
        }
        if (data?.measurementUnit?.length === 0) {
            isValid = false
            error.measurementUnit = "Measurement Unit is Required"
        }
        if (data?.doorNo?.length === 0) {
            isValid = false
            error.doorNo = "Door No is Required"
        }
        if (data?.address1?.length === 0) {
            isValid = false
            error.address1 = "Address is Required"
        }
        if (data?.address2?.length === 0) {
            isValid = false
            error.address2 = "Address is Required"
        }
        if (data?.landmark?.length === 0) {
            isValid = false
            error.landmark = "Landmark is Required"
        }
        if (data?.area?.length === 0) {
            isValid = false
            error.area = "Area is Required"
        }
        if (data?.city?.length === 0) {
            isValid = false
            error.city = "City is Required"
        }
        if (data?.state?.length === 0) {
            isValid = false
            error.state = "State is Required"
        }
        if (data?.country?.length === 0) {
            isValid = false
            error.country = "Country is Required"
        }
        if (data?.pincode?.length === 0) {
            isValid = false
            error.pincode = "Pincode is Required"
        }
        if (data?.businessPhone?.length === 0) {
            isValid = false
            error.businessPhone = "Business Phone is Required"
        }
        if (data?.mobilePhone?.length === 0) {
            isValid = false
            error.mobilePhone = "Mobile Phone is Required"
        }
        if (data?.website?.length === 0) {
            isValid = false
            error.website = "Website is Required"
        }
        if (data?.mail.length === 0) {
            isValid = false
            error.mail = "Mail- ID is required"
        }
        if (data?.mail.length > 0 && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data?.mail)) {
            isValid = false
            error.mail = "Please Fill Valid Mail ID"

        }

        setData({ ...data, error })
        return isValid
    }

    const clear = () => {
        setData({ ...initial })
        navigate(AppRoutes.propertiesDash);
    }

    const options = [
        {
            label: "one",
            value: "one",

        },
        {
            label: "two",
            value: "two",

        },
        {
            label: "three",
            value: "three",
        },
    ]











    return (
        <>
            <div className={classes.root}>
                <Stack direction="row" spacing={1} alignItems="center" className={classes.headerCreate}>
                    <Box className={classes.arrowColor}>
                        <DownArrow />
                    </Box>
                    <Typography className={classes.headerText}>
                        Create New Property
                    </Typography>
                </Stack>
                <div style={{ padding: "18px" }}>
                    {/* Top section */}
                    <Grid container spacing={2}>
                        <Grid item lg={2.5} md={12} sm={12} xs={12}>

                            <Box className={classes.leftSection}>
                                <Typography className={classes.labelTitle} textAlign={"center"}>PROPERTY IMAGE</Typography>

                                <Avatar className={classes.leftSectionAvatar} src='/images/NoPath.png'>
                                    {/* <UploadIcon /> */}
                                </Avatar>

                                <Button className={classes.leftSectionBtn} variant="outlined">Upload Image</Button>
                            </Box>

                        </Grid>
                        <Grid item lg={9.5} md={12} sm={12} xs={12}>
                            <Box className={classes.rightSection}>
                                <Typography className={classes.labelTitle}>
                                    PROPERTY DETAILS
                                </Typography>

                                <Grid container spacing={2}>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <SelectField
                                            label={"Company Name"}
                                            value={data?.companyName}
                                            onChange={(e) => updateState("companyName", e.target.value)}
                                            error={data.error.companyName.length > 0}
                                            errorMsg={data.error.companyName}
                                            options={options}
                                            required
                                        />

                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <InputField
                                            label={"Property Name"}
                                            value={data?.propertyName}
                                            onChange={(e) => updateState("propertyName", e.target.value)}
                                            error={data.error.propertyName.length > 0}
                                            errorMsg={data.error.propertyName}
                                            required
                                        />

                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>
                                        <SelectField
                                            label={"Payment Period"}
                                            required
                                            value={data?.paymentPeriod}
                                            onChange={(e) => updateState("paymentPeriod", e.target.value)}
                                            error={data.error.paymentPeriod.length > 0}
                                            errorMsg={data.error.paymentPeriod}
                                            options={options}
                                        />
                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>
                                        <SelectField
                                            label={"Status"}
                                            required
                                            value={data?.status} onChange={(e) => updateState("status", e.target.value)}
                                            error={data.error.status.length > 0}
                                            errorMsg={data.error.status} options={options} />
                                    </Grid>
                                    <Grid item lg={12} md={12} sm={12} xs={12}>
                                        <Typography className={classes.labelText}>Property Description</Typography>
                                        <ReactQuill theme="snow" value={value} onChange={setValue} />
                                    </Grid>
                                </Grid>
                            </Box>
                        </Grid>

                    </Grid>

                    {/* second section */}
                    <Grid container >
                        <Grid item sx={12} className={classes.middleSection}>

                            <Grid container spacing={1}>

                                <Grid item lg={2} md={6} sm={6} xs={12}>

                                    <InputField
                                        label={"Property Type"}
                                        required
                                        value={data?.propertyType} onChange={(e) => updateState("propertyType", e.target.value)}
                                        error={data.error.propertyType.length > 0}
                                        errorMsg={data.error.propertyType}
                                    />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>
                                    <SelectField
                                        label={"Property Purpose"}
                                        required
                                        value={data?.propertyPurpose}
                                        onChange={(e) => updateState("propertyPurpose", e.target.value)}
                                        error={data.error.propertyPurpose.length > 0}
                                        errorMsg={data.error.propertyPurpose}
                                        options={options} />
                                </Grid>
                                <Grid item lg={2} md={6} sm={6} xs={12}>

                                    <SelectField
                                        label={"Revenue Type"}
                                        required
                                        value={data?.revenueType}
                                        onChange={(e) => updateState("revenueType", e.target.value)}
                                        error={data.error.revenueType.length > 0}
                                        errorMsg={data.error.revenueType} options={options} />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>

                                    <SelectField
                                        label={"Measurement Unit"}
                                        required
                                        onChange={(e) => updateState("measurementUnit", e.target.value)}
                                        error={data.error.measurementUnit.length > 0}
                                        errorMsg={data.error.measurementUnit}
                                        options={options} />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>

                                    <InputField
                                        label={"Carpet Area"}
                                        required
                                        value={data?.carpetArea}
                                        onChange={(e) => updateState("carpetArea", e.target.value)}
                                        error={data.error.carpetArea.length > 0}
                                        errorMsg={data.error.carpetArea}
                                    />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>

                                    <InputField
                                        label={"Total Area"}
                                        required
                                        value={data?.totalArea} onChange={(e) => updateState("totalArea", e.target.value)}
                                        error={data.error.totalArea.length > 0}
                                        errorMsg={data.error.totalArea}
                                    />
                                </Grid>
                                <Grid item lg={2} md={6} sm={6} xs={12}>
                                    
                                    <DateShower
                                        label={"Year Built"}
                                        required
                                        value={data?.builtArea}
                                        onChange={(e) => updateState("builtArea", e)}
                                        error={data.error.builtArea.length > 0}
                                        errorMsg={data.error.builtArea}
                                    />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>
                                    
                                    <DateShower
                                        label={"Handover Date"}
                                        required
                                        value={data?.handoverDate}
                                        onChange={(e) => updateState("handoverDate", e)}
                                        error={data.error.handoverDate.length > 0}
                                        errorMsg={data.error.handoverDate}
                                    />
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>
                                    <Typography className={classes.labelText}>Public Listing</Typography>
                                    <Stack direction="row" spacing={0.7}>
                                        <Button variant="outlined" className={classes.outlinedButton}> None  </Button>
                                        <Button variant="outlined" className={classes.outlinedButton}> Private </Button>
                                        <Button variant="contained" className={classes.containedButton}> Public </Button>
                                    </Stack>
                                </Grid>

                                <Grid item lg={2} md={6} sm={6} xs={12}>
                                <Typography className={classes.labelText}>Pets Allowed</Typography>
                                <Checkbox checkedIcon={<CheckCircleIcon/>} icon={<RadioButtonUncheckedIcon/>} defaultChecked className={classes.checkBox} />
                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>

                    {/* third Section */}
                    <Box className={classes.thirdSection}>
                        <Typography className={classes.labelTitle}>ADDRESS</Typography>



                        <Grid container spacing={2}>
                            <Grid item lg={5} md={12} sm={12} xs={12}>
                                <Box className={classes.mapSection} >
                                    {/* <img className={classes.mapSectionImg} src='/images/googleMapsTask.png' /> */}
                                    
                                    <MapComponent  className={classes.mapSectionImg}/>
                                </Box>
                                
                            </Grid>
                            <Grid item lg={7} md={12} sm={12} xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item lg={2} md={4} sm={4} xs={12}>

                                        <InputField
                                            label={"Door Number"}
                                            required
                                            value={data?.doorNo}
                                            onChange={(e) => updateState("doorNo", e.target.value)}
                                            error={data.error.doorNo.length > 0}
                                            errorMsg={data.error.doorNo}
                                        />
                                    </Grid>
                                    <Grid item lg={5} md={4} sm={4} xs={12}>

                                        <InputField
                                            label={"Address Line 1"}
                                            required
                                            value={data?.address1}
                                            onChange={(e) => updateState("address1", e.target.value)}
                                            error={data.error.address1.length > 0}
                                            errorMsg={data.error.address1}
                                        />
                                    </Grid>
                                    <Grid item lg={5} md={4} sm={4} xs={12}>

                                        <InputField
                                            label={"Address Line 2"}
                                            required
                                            value={data?.address2}
                                            onChange={(e) => updateState("address2", e.target.value)}
                                            error={data.error.address2.length > 0}
                                            errorMsg={data.error.address2}
                                        />
                                    </Grid>

                                </Grid>

                                <Grid container spacing={1} marginTop={"10px"}>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <InputField
                                            label={"Landmark"}
                                            required
                                            value={data?.landmark}
                                            onChange={(e) => updateState("landmark", e.target.value)}
                                            error={data.error.landmark.length > 0}
                                            errorMsg={data.error.landmark}
                                        />
                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <SelectField
                                            label={"Area"}
                                            required
                                            value={data?.area}
                                            onChange={(e) => updateState("area", e.target.value)}
                                            error={data.error.area.length > 0}
                                            errorMsg={data.error.area} options={options}
                                        />
                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <SelectField
                                            label={"City"}
                                            required
                                            value={data?.city}
                                            onChange={(e) => updateState("city", e.target.value)}
                                            error={data.error.city.length > 0}
                                            errorMsg={data.error.city} options={options}
                                        />
                                    </Grid>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>
                                        <SelectField
                                            label={"State"}
                                            required
                                            value={data?.state}
                                            onChange={(e) => updateState("state", e.target.value)}
                                            error={data.error.state.length > 0}
                                            errorMsg={data.error.state}
                                            options={options}
                                        />
                                    </Grid>

                                </Grid>

                                <Grid container spacing={1} marginTop={"10px"}>
                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <SelectField
                                            label={"Country"}
                                            required
                                            value={data?.country}
                                            onChange={(e) => updateState("country", e.target.value)}
                                            error={data.error.country.length > 0}
                                            errorMsg={data.error.country} options={options}
                                        />
                                    </Grid>

                                    <Grid item lg={3} md={6} sm={6} xs={12}>

                                        <InputField
                                            label={"Pincode"}
                                            required
                                            value={data?.pincode}
                                            onChange={(e) => updateState("pincode", e.target.value)}
                                            error={data.error.pincode.length > 0}
                                            errorMsg={data.error.pincode}
                                        />






                                    </Grid>



                                </Grid>


                            </Grid>

                        </Grid>

                    </Box>

                    {/* fourth section */}
                    <Box className={classes.thirdSection}>
                        <Typography className={classes.labelTitle}>CONTACT & OTHER INFORMATION</Typography>

                        <Grid container spacing={2} >

                            <Grid item xl={3} lg={2} md={6} sm={6} xs={12}>

                                <SelectField
                                    label={"Business Phone"}
                                    required
                                    value={data?.businessPhone}
                                    onChange={(e) => updateState("businessPhone", e.target.value)}
                                    error={data.error.businessPhone.length > 0}
                                    errorMsg={data.error.businessPhone} options={options} />
                            </Grid>
                            <Grid item xl={3} lg={2} md={6} sm={6} xs={12}>

                                <SelectField
                                    label={"Mobile Phone"}
                                    required
                                    value={data?.mobilePhone}
                                    onChange={(e) => updateState("mobilePhone", e.target.value)}
                                    error={data.error.mobilePhone.length > 0}
                                    errorMsg={data.error.mobilePhone} options={options} />
                            </Grid>
                            <Grid item xl={3} lg={4} md={6} sm={6} xs={12}>

                                <InputField
                                    label={"Website"}
                                    required
                                    value={data?.website}
                                    onChange={(e) => updateState("website", e.target.value)}
                                    error={data.error.website.length > 0}
                                    errorMsg={data.error.website}
                                />



                            </Grid>
                            <Grid item xl={3} lg={4} md={6} sm={6} xs={12}>

                                <InputField
                                    label={"Email Address"}
                                    required
                                    value={data?.mail}
                                    onChange={(e) => updateState("mail", e.target.value)}
                                    error={data.error.mail.length > 0}
                                    errorMsg={data.error.mail}
                                />
                            </Grid>


                        </Grid>
                    </Box>
                </div>

                <Box className={classes.bottomSection}>
                    <Button onClick={() => clear()} className={classes.cancelBtn} variant='outlined' >Cancel</Button>
                    <Button onClick={() => submit()} className={classes.createBtn} variant='contained'>Create</Button>

                </Box>


            </div>

        </>
    )
}