import { makeStyles } from "@mui/styles";


export const PropertyCreationStyles = makeStyles((theme) => ({
    root:{
        // backgroundColor:"#F5F7FA ",
        // padding:"20px",
        backgroundColor:"#F5F7FA",
        //overflow:"auto",
        //height:"100vh",
    },
    leftSection:{
        backgroundColor:"white",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        height:"100%"

        
    },
    rightSection:{
        backgroundColor:"white",        
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
       
        [theme.breakpoints.up("sm")]: {
            height:"100%",
          },
          [theme.breakpoints.down('md')]: {
            height:"auto",
          }
        // marginLeft:"16px"
        
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },
    labelTitle:{
        color:"#4E5A6B",
        fontSize:"12px",
        marginBottom:"8px",
        fontFamily:"sans-serif",
        fontWeight:"600"
    },
    middleSection:{
        backgroundColor:"#FFFFFF",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px",
        width:"100%"
    },
    thirdSection:{
        backgroundColor:"#FFFFFF",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px"
    },
    leftSectionAvatar:{
        margin: "auto",
        width: "140px",
        height: "140px",
        marginTop: "12px",
    },
    leftSectionBtn:{
        margin:"auto",
        display:"flex",
        marginTop: "12px",
    },
    bottomBtns:{
        backgroundColor:"white",
        marginTop:"10px",
        position:"fixed",
         bottom:"0"

    },
    mapSection:{        
        // width:"100%",
        height:"auto",
        minHeight:"250px",
        // height:"100%",
        position:"relative",
        borderRadius:"8px"
    },
    mapSectionImg:{
        width:"100%",
        borderRadius:"8px",
        height: "100%",
        objectFit:"cover"

    },
    bottomSection:{
        backgroundColor:"white",
        padding:"12px",
        position:"sticky",
        display:"flex",
        justifyContent:"end",
        width:"100%",
        bottom:0,
        left:0,
        right:0
        
    },
    cancelBtn:{
        marginRight:"10px",
        
    },
    outlinedButton: {
        border: "1px solid #E4E8EE",
        color: "black",
        textTransform: "capitalize",
        borderRadius: "10px",
    },
  containedButton: {
        border: `1px solid ${theme.palette.secondary.main}`,
        backgroundColor: `${theme.palette.secondary.main}`,
        color: "white",
        textTransform: "capitalize",
        borderRadius: "10px",
        boxShadow: "none"
    },
    headerCreate: {
        boxShadow: "0px 10px 25px #0000000A",
        background: "#FFFFFF 0% 0% no-repeat padding-box",
        padding: "8px 22px",
        position: "sticky",
        top: 0,
        zIndex:"999"
    },
   arrowColor: {
        color: "#091B29",
        backgroundColor: "#E4E8EE",
        width: "24px",
        height: "24px",
        borderRadius: "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },

 headerText: {
        color: "#091B29",
        fontSize: "16px",
        fontWeight: "bold",
    },
    checkBox:{
        borderRadius:"50%"
    }
    

    
    

}));