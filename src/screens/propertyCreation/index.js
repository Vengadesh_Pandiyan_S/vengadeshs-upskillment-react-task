import React from "react";
import { PropertyCreation } from "./propertyCreation";
import { withNavBars } from "./../../HOCs";

class PropertyCreationParent extends React.Component {
  render() {
    return <PropertyCreation />;
  }
}

export default withNavBars(PropertyCreationParent);
