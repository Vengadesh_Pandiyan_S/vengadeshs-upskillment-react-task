import { makeStyles } from "@mui/styles";


export const PropertyViewStyles = makeStyles((theme) => ({
    root:{
        padding:"30px",
        backgroundColor:"#F5F7FA",
        // overflow:"auto",
        // height:"100vh",
    },

    topSection:{
        backgroundColor:"white",
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        marginTop:"10px",
        
    },
    rightSection:{
        backgroundColor:"white",        
        padding:"10px",
        boxShadow:"0px 10px 25px #0000000A",
        borderRadius:"8px",
        height:"264px",
        // marginLeft:"16px"
        
    },
    sqft:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
        marginLeft:"8px"
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },
    labelTitle:{
        color:"#4E5A6B",
        fontSize:"12px",
        paddingBottom:"16px",
        fontFamily:"sans-serif",
        fontWeight:"600"
    },
    text:{
        color:"#091B29",
        fontSize:"14px",
        // paddingBottom:"8px",
        fontFamily:"sans-serif",
        fontWeight:"500"
    },
    addressTitle:{
        color:"#091B29",
        fontSize:"12px",
        fontFamily:"sans-serif",
        fontWeight:"600",
        display:"flex",
        alignItems:"center"

    },
    addressText:{
        color:"#091B29",
        fontSize:"14px",
        fontFamily:"sans-serif",
        fontWeight:"500",
        paddingTop:"12px",
        // word-break: break-word;
        wordBreak:"break-word",
    },
    addressCard:{
        border:"1px solid #E4E8EE",
        borderRadius:"8px",
        height: "100%",
        padding: "16px",
    },
    latTitle:{
        color:"#98A0AC",
        fontSize:"12px",
        fontFamily:"sans-serif",
        // fontWeight:"500",


    },
    latText:{
        color:"#091B29",
        fontSize:"12px",
        fontFamily:"sans-serif",
        paddingTop:"16px"
        
    },
    leftSectionAvatar:{
        margin: "auto",
        width: "140px",
        height: "140px",
        
    },
    mapSection:{        
        // width:"100%",
        height:"auto",
        minHeight:"250px",
        // height:"100%",
        position:"relative",
        borderRadius:"8px"
    },
    mapSectionImg:{
        width:"100%",
        borderRadius:"8px",
        height: "100%",
        objectFit:"cover"

    },
   
    
    

}));