import React from "react";
import { PropertyView } from "./propertyView";
import { withNavBars } from "./../../HOCs";

class PropertyViewParent extends React.Component {
  render() {
    return <PropertyView />;
  }
}

export default withNavBars(PropertyViewParent);
