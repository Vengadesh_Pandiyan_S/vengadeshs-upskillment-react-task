
import React, { useState } from 'react';
import { Button, Card, Grid, Stack, TextField, Typography, } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { LocalStorageKeys } from '../../utils';
import { AppRoutes } from '../../router/routes';
import { AlertContext } from "../../contexts";
import {
    AlertProps,
} from "../../utils";
import { Box } from '@mui/system';
import { useNavigate } from 'react-router-dom';
import { loginStyle } from './style';


const initial = {
    mail: '',
    password: '',
    error: {
        mail: '',
        password: '',
    }
}
export const Login = props => {
    const navigate = useNavigate();
    const classes = loginStyle();
    const [data, setData] = useState({ ...initial })
    const alert = React.useContext(AlertContext);
    const onLogin = () => {
        if (formValidation()) {
            localStorage.setItem(LocalStorageKeys.authToken, "authtoken");


            navigate(AppRoutes.propertiesDash);

            alert.setSnack({
                ...alert,
                open: true,
                severity: AlertProps.severity.success,
                msg: "Login Successfully",
                horizontal: AlertProps.horizontal.center,
            });
        }
    }
    const formValidation = () => {

        let isValid = true;
        let error = data.error

        if (data.mail !== "vengadesh@crayond.co") {
            isValid = false;
            // error.mail = "Mail id is not valid";
            alert.setSnack({
                ...alert,
                open: true,
                severity: AlertProps.severity.error,
                msg: "Username or Password is wrong",
                horizontal: AlertProps.horizontal.center,
            });
        }
        if (data.password !== "12345") {
            // error.password = "Password is not valid";
            isValid = false;
            alert.setSnack({
                ...alert,
                open: true,
                severity: AlertProps.severity.error,
                msg: "Username or Password is wrong",
                horizontal: AlertProps.horizontal.center,
            });
        }
        setData({ ...data, error })
        return isValid;
    }
    const updateState = (key, value) => {
        console.log(key, value);
        let error = data.error
        error[key] = ""
        setData({ ...data, [key]: value })
    }
    return (
        <div className={classes.root}>
            {/* <Grid container>
        
                <Grid container>
                    <Grid >
                        <Card className={classes.loginBg}>
                            <Grid marginBottom={"20px"} >
                                <TextField fullWidth id="outlined-basic" label="Mail-id" variant="outlined" type='text' value={data.mail} onChange={(e) => updateState('mail', e.target.value)} />
                                <div className={classes.errorText}>
                                    {data.error.mail}
                                </div>
                            </Grid>
                            <Grid marginBottom={"20px"} >
                                <TextField fullWidth id="outlined-basic" label="Password" variant="outlined" type='password' value={data.password} onChange={(e) => updateState('password', e.target.value)} />
                                <div className={classes.errorText}>
                                    {data.error.password}
                                </div>
                            </Grid>
                            <Grid
                                style={{ width: "100%" }}
                            >
                                <Button
                                    variant={"contained"}
                                    color={"secondary"}
                                    onClick={onLogin}
                                    marginTop="10px"
                                    fullWidth
                                    className={classes.btn}
                                >
                                    LogIn
                                </Button>
                            </Grid>
                            <Grid marginTop="20px">
                                <Box style={{ display: "flex", alignItems: "center", margin: "auto", textAlign:"center", justifyContent:"space-between", width:"84%"}}>
                                        <Typography className={classes.powerTitle}>Powered by </Typography>
                                    
                                    <Box display={"flex"} style={{ width: "10%" }}>
                                        <img style={{ width: "100%" }} src='/favicon.ico' alt='Crayond'></img>
                                    </Box>
                                    
                                        <Typography className={classes.favTitle}>Property Automate</Typography>
                                    
                                </Box>
                            </Grid>

                        </Card>
                    </Grid>
                    {/* <Grid 
            item xs={12} sm={12} md={12} lg={12}
            className={classes.loginBg}
        >
        <Grid marginBottom={"20px"} >
            <TextField fullWidth id="outlined-basic" label="Mail-id" variant="outlined" type='text' value={data.mail} onChange={(e) => updateState('mail', e.target.value)} />
            <div className={classes.errorText}>
                {data.error.mail}
            </div>
        </Grid>
        <Grid marginBottom={"20px"} >
            <TextField fullWidth id="outlined-basic" label="Password" variant="outlined" type='password' value={data.password} onChange={(e) => updateState('password', e.target.value)} />
            <div className={classes.errorText}>
            {data.error.password}
            </div>
        </Grid>
        <Grid 
            
            style={{marginTop:"10px"}}
        >
            <Button
                variant={"contained"}
                color={"secondary"}
                onClick={onLogin}
                marginTop="10px"
                fullWidth
                className={classes.btn}
                
            >
                LogIn
            </Button>
        </Grid>
        </Grid> */}
                {/* </Grid> */}
            {/* </Grid>  */}

            <Grid container>
            {/* login card */}
            <Grid item xs={12} sm={12} md={12} lg={12} >
                <Stack className={classes.backgroundImg}>
                    <Box className={classes.card}>
                        {/* title */}
                        <Typography className={classes.signIn}>
                            Sign In
                        </Typography>
                        {/* title */}
                        {/* Inputbox */}
                        <Stack className={classes.inputBox} >
                            <TextField fullWidth id="outlined-basic" label="Mail-id" variant="outlined" type='text' value={data.mail} onChange={(e) => updateState('mail', e.target.value)} />
                                <div className={classes.errorText}>
                                    {data.error.mail}
                                </div>
                        </Stack>
                        {/* Inputbox */}

                        {/* password */}
                        <Stack className={classes.passWord} >
                        <TextField fullWidth id="outlined-basic" label="Password" variant="outlined" type='password' value={data.password} onChange={(e) => updateState('password', e.target.value)} />
                                <div className={classes.errorText}>
                                    {data.error.password}
                                </div>
                        </Stack>
                        {/* password */}

                        {/* forget password */}
                        <Typography className={classes.forget}>
                            Did you forget your password? <span className={classes.clickHere}>Click Here</span>
                        </Typography>
                        {/* forget password */}

                        {/* powered by */}
                        <Stack direction="row" spacing={1} alignItems="center" justifyContent="center" className={classes.sponsors}>
                            <Typography className={classes.powerBy}>
                                Powered by
                            </Typography>
                            <img src="./images/logo@2x.png" alt="logo" className={classes.squareLogo} />
                            <Typography className={classes.propAuto}>
                                Property Automate
                            </Typography>
                        </Stack>
                        {/* powered by */}

                        {/* login button */}
                        <Button
                                    variant={"contained"}
                                    color={"secondary"}
                                    onClick={onLogin}
                                    marginTop="10px"
                                    fullWidth
                                    className={classes.btn}
                                >
                                    LogIn
                                </Button>
                        {/* login button */}
                    </Box>
                </Stack>
            </Grid>
            {/* login card */}
            </Grid>

        </div>
    )
}
