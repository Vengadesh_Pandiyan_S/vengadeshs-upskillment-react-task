import { makeStyles } from "@mui/styles";


export const loginStyle = makeStyles((theme) => ({
    root: {
        textAlign: "center",
        height: "100vh",
        backgroundImage: `url("/images/loginBackGroundImage.png")`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "fixed",

    },
    errorText: {
        color: "red",
        marginRight: "110px",
        fontSize: "12px",
    },
    loginBg: {
        borderRadius: "12px",
        padding: "40px 30px 40px 30px",
        marginTop: "100px",
        position: "fixed",
        right: "100px",
        top: "60px",
        alignItems: "center"
    },
    title: {
        color: "white",
        marginBottom: "40px",
        textAlign: "center",
        marginTop: "30px",
        fontSize: "32px",
        fontFamily: "sans-serif",
        fontWeight: 700,
    },
    favTitle: {
        color: "#4E5A6B",
        // marginRight:"110px",
        fontSize: "12px",
        // marginLeft:"10px",
        fontWeight: 500,
        fontFamily: "sans-serif"
    },
    powerTitle: {
        color: "#98A0AC",
        // marginRight:"110px",
        fontSize: "10px",
        // marginLeft:"10px",
        fontWeight: 500,
        fontFamily: "sans-serif"

    },
    card: {
        backgroundColor: "#FFFFFF",
        boxShadow: "0px 8px 24px #0000001F",
        borderRadius: "16px",
        opacity: 1,
        padding: "30px",
        marginRight: "105px",
        width: "416px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "0px",
            width: "auto",
        }
    },
    backgroundImg:{
        alignItems:"end",
        backgroundImage:`url("images/loginBackGroundImage.png")`,
        backgroundRepeat: "no-repeat",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        [theme.breakpoints.down("sm")]: {
            alignItems:"center",
        }
    },
    signIn: {
        fontSize: "24px",
        textAlign: "left",
        marginBottom: "32px",
        fontWeight: "bold"
    },
    inputBox: {
        marginBottom: "30px"
    },
    passWord: {
        marginBottom: "8px"
    },
    forget: {
        fontSize: "12px",
        textAlign: "right",
        marginBottom: "50px",
        marginTop: "8px"
    },
    clickHere: {
        color: "#5078E1",
        cursor: "pointer"
    },
    powerBy: {
        color: "#98A0AC",
        fontSize: "10px"
    },
    propAuto: {
        color: "#4E5A6B",
        fontSize: "12px"
    },
    sponsors:{
        marginBottom:"24px"
    },
    login:{
        borderRadius:"12px",
        backgroundColor:"#5078E1",
        textTransform: "initial"
    },
    squareLogo:{
        width: "24px",
        height: "17px",
    },
    
    
    
}
    
    

));