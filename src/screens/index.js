export { default as NotFound } from "./notFound";
export { default as Home } from "./home";
export { default as Login } from "./login";
export { default as PropertyDashboard } from "./dashboard";
export { default as Collabo } from "./collabo";
export { default as PropertyCreation } from "./propertyCreation";
export { default as PropertiesDash } from "./propertiesList";
export { default as PropertyView } from "./propertyView";
export { default as PersonalForm } from "./personalForm";
export { default as PersonalFormTable } from "./personalFormTable";
export { default as PropertyViewMob } from "./propertyViewMob";
export { default as PropertiesDashMob } from "./propertiesListMob";
export { default as PropertyCreationMob } from "./propertyCreationMob";
export { default as PropertyTable } from "./propertyTable";
