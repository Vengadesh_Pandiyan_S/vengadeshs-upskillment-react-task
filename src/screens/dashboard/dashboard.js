import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from "@mui/styles";
import { SmallCards, BigCards, RectangleCards } from '../../components';
import { dashboardStyles } from './style';




const useStyles = makeStyles((theme) => ({
    root: {

    }
}))

export const PropertyDashboard = props => {
    const classes = dashboardStyles();

    const data = [
        {
            id: 0,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 1,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 3,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 4,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
    ]
    const datas = {
        headTitle: "Property Types",
        headIcon: "one",
        vacantValue: 25,
        vacantText: "Vacant",
        occupiedValue: 25,
        occupiedText: "Occupied",
        reservedValue: 30,
        reservedText: "Reserved",
        listedValue: 20,
        listedText: "Listed",
        image: "images/goal.svg",
        icon: "images/icons8-enlarge-48.png"
    }
    const rectCards = [
        {
            id: 0,
            area: "Total Area",
            areaVal: 2451,
        },
        {
            id: 1,
            area: "Carpet Area",
            areaVal: 3451,
        },
    ]

    const dataTwelve = [
        {
            id: 0,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 1,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 2,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },

        // {
        //     id: 3,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // },
        // {
        //     id: 4,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // },
        // {
        //     id: 5,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // },
        // {
        //     id: 6,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // },
        // {
        //     id: 7,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // },
        // {
        //     id: 8,
        //     number: 14,
        //     title: "Active Properties",
        //     image: "images/Leads.png"
        // }
    ]
    const datasFour = [
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed"
        },
    ]





    return (
        <div className={classes.root}>
            {/* first row */}
            <Grid container spacing={2} >
                {/*first  half starts*/}
                <Grid item xs={12} sm={12} md={12} lg={6}>
                    <Grid container spacing={2}>
                        {/* left section starts */}
                        <Grid item lg={3} md={3} sm={3} xs={12}>
                            <Grid container spacing={2}>
                                {dataTwelve?.map((item) => {
                                    return (
                                        <Grid item xs={12}>
                                            <SmallCards data={item} />
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </Grid>


                        {/* middle section starts */}
                        <Grid item lg={6} md={6} sm={6} xs={12}>
                            <BigCards data={datas} chart />

                        </Grid>


                        {/* right section starts */}
                        <Grid item lg={3} md={3} sm={3} xs={12}>
                            <Grid container spacing={2}>
                                {dataTwelve?.map((item) => {
                                    return (
                                        <Grid item xs={12}>
                                            <SmallCards data={item} />
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </Grid>


                    </Grid>
                </Grid>


                {/* second half starts*/}
                <Grid item xs={12} sm={12} md={12} lg={6}>
                    <Grid container spacing={2} rowSpacing={2}>
                        {/* left section */}
                        <Grid item lg={6} md={6} sm={6} xs={12}>
                            <BigCards data={datas} chart />

                        </Grid>

                        {/* right section */}
                        <Grid item lg={6} md={6} sm={6} xs={12}>
                            <Grid container spacing={2}>

                                <Grid item xs={12}>
                                    <Grid container spacing={2}>
                                        {data?.map((item) => {
                                            return (
                                                <Grid item xs={6}>
                                                    <SmallCards data={item} />
                                                </Grid>

                                            )
                                        })}
                                    </Grid>


                                </Grid>

                                <Grid item xs={12}>
                                    <Grid container spacing={2}>
                                        {
                                            rectCards?.map((item) => {
                                                return (
                                                    <Grid item xs={12}>
                                                        <RectangleCards data={item} />
                                                    </Grid>


                                                )
                                            })
                                        }
                                    </Grid>



                                </Grid>



                            </Grid>


                        </Grid>


                    </Grid>

                </Grid>

            </Grid>


            





        </div>
    )
}