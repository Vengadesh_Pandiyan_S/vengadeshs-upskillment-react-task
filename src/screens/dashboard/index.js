import React from "react";
import { PropertyDashboard } from "./dashboard";
import { withNavBars } from "./../../HOCs";

class PropertyDashboardParent extends React.Component {
  render() {
    return <PropertyDashboard />;
  }
}

export default withNavBars(PropertyDashboardParent);
