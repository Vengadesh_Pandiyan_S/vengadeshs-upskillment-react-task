import { makeStyles } from "@mui/styles";


export const PropertyTableStyles = makeStyles((theme) => ({
    root:{
        backgroundColor:"#F5F7FA ",
        // padding:"16px",
        height:"91vh",
        
        
    },
    tableSection:{
        backgroundColor:"white",
        padding:"16px",
        borderRadius:"8px",
        height:"80vh",
        boxShadow:"0px 3px 30px #5C86CB2E",
    },
    tabBox:{
        backgroundColor: "white",
        borderRadius: "4px",
        height: "100%",
        maxHeight:"420px",
        boxShadow:"0px 3px 30px #5C86CB2E",

    },
    tableBox:{
        backgroundColor: "white",
        borderRadius: "4px",  
        height:"100%",
        overflow:"auto",
        maxHeight:"450px",
        boxShadow:"0px 3px 30px #5C86CB2E",

    },
    topSection:{
        alignItems:"center",
        marginBottom:"20px"
    },
    filterIcon:{
        float:"right",
        marginTop:"4px"
    },
    headerCreate: {
        boxShadow: "0px 10px 25px #0000000A",
        background: "#FFFFFF",
        padding: "10px 22px",
    },
    headerText: {
        color: "#091B29",
        fontSize: "16px",
        fontWeight: "bold",
    },

   

    
    borderRightStyling: {
        height: "21px",
        margin: "auto 0 auto 0 !important"
    },
    parentRequest: {
        borderRadius: "4px",
        backgroundColor: "#F5F7FA",
        padding: "14px",
        display: "inline-flex",
        // width:"45%"
    },
    reqHead:{
        color:"#4E5A6B",
        fontSize:"14px",
    },
    requestSection:{
        padding:"12px 12px 2px"
    },
    reqHeadSection:{
        display:"flex",
        alignItems:"center" ,
        justifyContent:"space-between",
        marginBottom:"8px",       
    },
    fieldSection:{
        marginBottom:"8px"
    },
    tabsDatasSection:{
        overflow:"auto",
        height:"100%",
        maxHeight:"160px"
    }
    
    
    

}));