import React, { useState } from 'react';
import { Grid, Stack, Typography } from '@mui/material';
import { CustomizeTable, InputField, } from '../../components';
import { PropertyTableStyles } from './style';
import { Box } from '@mui/system';
import { AppRoutes } from '../../router/routes';
import { useNavigate } from 'react-router-dom';
import { PropertyTableType, PropertyTableheading, PropertyTablepath, PropertyTablerow } from "../../utils"
import { FilterIcon } from '../../assets';






const initial = {
    search:""
}


export const PropertyTable = props => {
    const navigate = useNavigate();
    // const [activeTab, setActiveTab] = useState(1);
    const [inputField, SetInputField] = useState("")
    const [data, setData] = useState({...initial});
    const [tableData, setTableData] = useState(PropertyTablerow)
    const [searchString, setsearchString] = useState("")

    const classes = PropertyTableStyles();


    // const data = [
    //     {
    //         id: 0,
    //         number: 14,
    //         title: "Active Properties",
    //         image: "images/ActiveProperties.svg"
    //     },
    //     {
    //         id: 1,
    //         number: 20,
    //         title: "Blocks",
    //         image: "images/Leads.svg"
    //     },
    //     {
    //         id: 3,
    //         number: 18,
    //         title: "Floors",
    //         image: "images/Floors.svg"
    //     },
    //     {
    //         id: 4,
    //         number: 10,
    //         title: "Residents",
    //         image: "images/Residents.png"
    //     },
    //     {
    //         id: 5,
    //         number: 12,
    //         title: "Active Unit",
    //         image: "images/Active Unit.png"
    //     },
    //     {
    //         id: 6,
    //         number: 7,
    //         title: "Vaccant",
    //         image: "images/VacantCard.png"
    //     },
    //     {
    //         id: 7,
    //         number: 6,
    //         title: "Reserved",
    //         image: "images/Reserved.svg"
    //     },
    //     {
    //         id: 8,
    //         number: 20,
    //         title: "Occupied",
    //         image: "images/Occupied.png"
    //     },
    // ]

    // const datas = [
    //     {
    //         headTitle: "Property Types",
    //         // headIcon: "one",
    //         // vacantValue: 25,
    //         // vacantText: "Vacant",
    //         // occupiedValue: 25,
    //         // occupiedText: "Occupied",
    //         // reservedValue: 30,
    //         // reservedText: "Reserved",
    //         // listedValue: 20,
    //         // listedText: "Listed",
    //         image: "images/propertyType.png",
    //         icon: "images/icons8-enlarge-48.png"
    //     },
    //     {
    //         headTitle: "Unit Category",
    //         // headIcon: "one",
    //         // vacantValue: 25,
    //         // vacantText: "Vacant",
    //         // occupiedValue: 25,
    //         // occupiedText: "Occupied",
    //         // reservedValue: 30,
    //         // reservedText: "Reserved",
    //         // listedValue: 20,
    //         // listedText: "Listed",
    //         image: "images/unitCat.png",
    //         icon: "images/icons8-enlarge-48.png"
    //     },
    //     {
    //         headTitle: "Vacant Units By Property",
    //         // headIcon: "one",
    //         // vacantValue: 25,
    //         // vacantText: "Vacant",
    //         // occupiedValue: 25,
    //         // occupiedText: "Occupied",
    //         // reservedValue: 30,
    //         // reservedText: "Reserved",
    //         // listedValue: 20,
    //         // listedText: "Listed",
    //         image: "images/vacant.png",
    //         icon: "images/icons8-enlarge-48.png"
    //     },
    //     {
    //         headTitle: "Property Types",
    //         // headIcon: "one",
    //         // vacantValue: 25,
    //         // vacantText: "Vacant",
    //         // occupiedValue: 25,
    //         // occupiedText: "Occupied",
    //         // reservedValue: 30,
    //         // reservedText: "Reserved",
    //         // listedValue: 20,
    //         // listedText: "Listed",
    //         image: "images/totalARea.png",
    //         icon: "images/icons8-enlarge-48.png"
    //     }
    // ]

    const handleEvent = (event) => {
        SetInputField(event.target.value);
    }

    const createIcon = () => {
        navigate(AppRoutes.propertyCreation);
    }

    const updateState = (key, value) => {
        let error = data.error
        error[key] = ""
        setData({ ...data, [key]: value, error });
    }

    const tableFilter = (search)=>{
        setsearchString(search)
        let b =  PropertyTablerow.filter(function (str) { return str.propertyName.includes(search) });
        setTableData(b)
    }


    const tabsData = [
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "10 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Drinage Repair",
            requestId: "K-F01-U277",
            date: "20 NOV 22",
        },
        {
            requestName: "Maintenance",
            title: "Plumbing Repair",
            requestId: "K-F01-U277",
            date: "30 OCT 22",
        },
    ]
// bun*a
    // const tabs = [
    //     {
    //         label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Gendral</Typography>,
    //         value: 1,
    //         body: <div style={{ padding: "0px" }}>

    //             <Box className={classes.reqHeadSection}>
    //                 <Typography className={classes.reqHead}>General Request</Typography>
    //                 <Button>View all</Button>
    //             </Box>

    //             <Box className={classes.fieldSection}>
    //                 <InputField handleChange={handleEvent} value={inputField} />
    //             </Box>


    //             <Box className={classes.tabsDatasSection}>
    //                 {tabsData?.map?.((item) => {
    //                     return (
    //                         <Box marginBottom={"8px"}>
    //                             <RequestCard data={item} />
    //                         </Box>


    //                     )
    //                 })}
    //             </Box>
    //         </div>
    //     },
    //     {
    //         label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Maintenance</Typography>,
    //         value: 2,

    //         body: <div style={{ padding: "0px" }}>

    //             <Box className={classes.reqHeadSection}>
    //                 <Typography className={classes.reqHead}>Maintaince Request</Typography>
    //                 <Button>View all</Button>
    //             </Box>

    //             <Box className={classes.fieldSection}>
    //                 <InputField handleChange={handleEvent} value={inputField} />
    //             </Box>

    //             <Box className={classes.tabsDatasSection}>
    //                 {tabsData?.map?.((item) => {
    //                     return (
    //                         <Box marginBottom={"8px"}>
    //                             <RequestCard data={item} />
    //                         </Box>
    //                     )
    //                 })}
    //             </Box>

    //         </div>
    //     },
    // ]


    const handleIcon = (type, data) => {
        if (type === "edit") { }
        if (type === "delete") { }
        if (type === "view") { }
    }






    return (
        <div className={classes.root}>
            <Stack direction="row" spacing={1} alignItems="center" className={classes.headerCreate}>

                <Typography className={classes.headerText}>
                    Properties
                </Typography>
            </Stack>

            <div style={{ padding: "16px" }}>
                <Grid className={classes.tableSection}>
                    <Grid container className={classes.topSection}>
                        <Grid item lg={4} md={8} sm={8} xs={6} >
                            {/* <InputField /> */}
                            <InputField
                                // label={"Property Name"}
                                value={searchString}
                                onChange={(e) => tableFilter(e.target.value)}
                            />

                        </Grid>
                        <Grid item lg={8} md={4} sm={4} xs={6} >
                            <FilterIcon sx={{color:"white"}} className={classes.filterIcon} />

                        </Grid>
                    </Grid>

                    <Box className={classes.tableBox}>

                        <CustomizeTable
                            heading={PropertyTableheading}
                            rows={tableData}
                            path={PropertyTablepath}
                            dataType={PropertyTableType}
                            handleIcon={handleIcon}
                        // showpagination={true}
                        // count="2"
                        // showpdfbtn={false}
                        // showexcelbtn={false}
                        // showSearch={false}
                        // handleIcon={handleIcon}
                        // onClick={(data) => console.log("")}
                        // onUnitClick={onUnitClick}
                        // tableType="no-side"
                        // view={true}
                        // handlePagination={handlePagination}
                        // handleChangeLimit={handleChangeLimit}
                        // totalRowsCount={propertyList?.totalRowsCount}
                        // page={page}
                        // limit={limit}
                        // height={'calc(100vh - 290px)'}

                        />
                    </Box>

                </Grid>
            </div>





        </div>
    )
}