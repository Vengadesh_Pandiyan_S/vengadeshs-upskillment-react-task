import React from "react";
import { withNavBars } from "./../../HOCs";
import { PropertyTable } from "./propertyTable";

class PropertyTableParent extends React.Component {
  render() {
    return <PropertyTable />;
  }
}

export default withNavBars(PropertyTableParent);
