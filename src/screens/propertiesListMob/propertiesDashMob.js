import React, { useState } from 'react';
import { Button, Container, Divider, Grid, Typography } from '@mui/material';
import { makeStyles } from "@mui/styles";
import { BigCards, DataTabs, InputField, RequestCard, SelectField, SmallCards } from '../../components';
import { PropertiesDashMobStyles } from './style';
import { Box } from '@mui/system';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { AppRoutes } from '../../router/routes';
import { useNavigate } from 'react-router-dom';







export const PropertiesDashMob = props => {
    const navigate = useNavigate();
    const [activeTab, setActiveTab] = useState(1);
    const [inputField, SetInputField] = useState("")

    const classes = PropertiesDashMobStyles();


    const data = [
        {
            id: 0,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 1,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 3,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 4,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 5,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 6,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 7,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
        {
            id: 8,
            number: 14,
            title: "Active Properties",
            image: "images/Leads.png"
        },
    ]

    const datas = [
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed",
            image: "images/goal.svg",
            icon: "images/icons8-enlarge-48.png"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed",
            image: "images/goal.svg",
            icon: "images/icons8-enlarge-48.png"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed",
            image: "images/goal.svg",
            icon: "images/icons8-enlarge-48.png"
        },
        {
            headTitle: "Property Types",
            headIcon: "one",
            vacantValue: 25,
            vacantText: "Vacant",
            occupiedValue: 25,
            occupiedText: "Occupied",
            reservedValue: 30,
            reservedText: "Reserved",
            listedValue: 20,
            listedText: "Listed",
            image: "images/goal.svg",
            icon: "images/icons8-enlarge-48.png"
        }
    ]

    const handleEvent = (event) => {
        SetInputField(event.target.value);
    }

    const createIcon = ()=>{
        navigate(AppRoutes.propertyCreationMob);
    }


    const tabsData = [
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
        {
            requestName: "Maintenance",
            title: "Water Lekage Repair",
            requestId: "K-F01-U277",
            date: "22 JAN 21",
        },
    ]

    const tabs = [
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Gendral</Typography>,
            value: 1,
            body: <div>
                <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"} marginBottom={"10px"}>
                    <Typography>General Request</Typography>
                    <Button>View all</Button>
                </Box>

                <InputField handleChange={handleEvent} value={inputField} />


                <Box marginTop={"10px"}>
                    {tabsData?.map?.((item) => {
                        return (
                            <Box marginBottom={"8px"}>
                                <RequestCard data={item} />
                            </Box>
                            

                        )
                    })}
                </Box>
            </div>
        },
        {
            label: <Typography variant="body2" gutterBottom component="div" sx={{ fontFamily: "crayond_semibold", fontSize: "14px", textTransform: "capitalize" }}>Maintenance</Typography>,
            value: 2,
            body: <div>
                {tabsData?.map?.((item) => {
                    return (
                        <RequestCard data={item} />
                    )
                })}
            </div>
        },
    ]









    return (
        <div className={classes.root}>

            <Container maxWidth="sm">

            {/* top section */}
            <Grid container spacing={2} marginBottom={"8px"}>
                {data?.map((item) => {
                    return (
                        <Grid item lg={6} md={4} sm={4} xs={12}>
                            <SmallCards data={item} />
                        </Grid>
                    )
                })}
            </Grid>

            {/* second section */}
            <Grid container spacing={2} marginTop={"8px"}>
                {
                    datas?.map?.((item) => {
                        return (
                            <Grid item lg={12} md={6} sm={6} xs={12}>
                                <BigCards data={item} />
                            </Grid>
                        )
                    })

                }

            </Grid>


            <Grid container spacing={2} marginTop={"8px"} >
                <Grid item lg={12} md={6} sm={12} xs={12} >

                    <Box className={classes.tabBox}>
                        {/* <Box className={classes.reqHead} display={"flex"}>
                            <Typography>hi</Typography>
                            <Divider orientation="vertical" flexItem />
                            <Typography>hi</Typography>
                        </Box> */}
                        <DataTabs tabs={tabs} activeTab={activeTab} handleChangeTab={(val) => setActiveTab(val)} />
                    </Box>
                </Grid>
                <Grid item lg={12} md={6} sm={12} xs={12}>
                    <Box className={classes.tabBox}>
                        <DataTabs tabs={tabs} activeTab={activeTab} handleChangeTab={(val) => setActiveTab(val)} />
                    </Box>
                </Grid>

            </Grid>

            <Box>
                
                <AddCircleIcon onClick={createIcon} className={classes.AddIcon}/>
            </Box>
            </Container>
        </div>
    )
}