import React from "react";
import { withNavBarsPWA } from "./../../HOCs";
import { PropertiesDashMob } from "./propertiesDashMob";

class PropertiesDashMobParent extends React.Component {
  render() {
    return <PropertiesDashMob />;
  }
}

export default withNavBarsPWA(PropertiesDashMobParent);


                                
