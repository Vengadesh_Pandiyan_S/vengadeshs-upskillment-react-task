import { makeStyles } from "@mui/styles";


export const PropertiesDashMobStyles = makeStyles((theme) => ({
    root:{
        backgroundColor:"#F5F7FA ",
        padding:"14px",
        overflow:"auto",
        height:"100vh",
    },
    tabBox:{
        backgroundColor: "white",
        borderRadius: "4px",
        // height: "112px",
        boxShadow:"0px 3px 30px #5C86CB2E"

    },
    reqHead:{
        backgroundColor:"red",
        width:"50%"
    },
    AddIcon:{
        color:"#0065E6",
        fontSize:"60px",
        position:"fixed",
        bottom:"20px",
        right:"27%",
        cursor:"pointer"
    }
    
    

}));