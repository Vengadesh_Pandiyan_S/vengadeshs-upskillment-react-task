import React from "react";
import { Collabo } from "./collabo";
import { withNavBars } from "./../../HOCs";

class CollaboParent extends React.Component {
  render() {
    return <Collabo />;
  }
}

export default withNavBars(CollaboParent);
