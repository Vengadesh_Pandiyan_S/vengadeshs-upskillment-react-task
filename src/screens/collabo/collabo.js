import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from "@mui/styles";
import { SmallCards, BigCards, RectangleCards, CollaboCards } from '../../components';
import { collaboStyles } from './style';






export const Collabo = props => {
    const classes = collaboStyles();

    const data = [
        {
            date:"Aug, 2022 11:59 PM",
            title:"Owner assignment for property",
            leftCard:"8854-Development",
            rightCard:"#property-automate",
            completed:"0%",
            estHrs:"0.5hrs"
        },
        {
            date:"Aug, 2022 11:59 PM",
            title:"Owner assignment for property",
            leftCard:"8854-Development",
            rightCard:"#property-automate",
            completed:"0%",
            estHrs:"0.5hrs"
        },
        {
            date:"Aug, 2022 11:59 PM",
            title:"Owner assignment for property",
            leftCard:"8854-Development",
            rightCard:"#property-automate",
            completed:"0%",
            estHrs:"0.5hrs"
        },
        {
            date:"Aug, 2022 11:59 PM",
            title:"Owner assignment for property",
            leftCard:"8854-Development",
            rightCard:"#property-automate",
            completed:"0%",
            estHrs:"0.5hrs"
        },
        {
            date:"Aug, 2022 11:59 PM",
            title:"Owner assignment for property",
            leftCard:"8854-Development",
            rightCard:"#property-automate",
            completed:"0%",
            estHrs:"0.5hrs"
        },
]

    





    return (
        <div className={classes.root}>

            <Grid container spacing={2}>
                
                    
                { 
                data?.map((item)=>{
                    return(
                        <Grid item lg={3} ms={3} sm={6} xs={12}>
                            <CollaboCards data={item} />
                        </Grid>
                        
                        

                        )
                })

            }

                

            </Grid>
            
            
           





        </div>
    )
}