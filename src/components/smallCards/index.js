import React from 'react';
import { Avatar, Box, Card, CardContent, Grid, Stack, Typography } from '@mui/material';
import { smallCardsStyles } from './style';





export const SmallCards = (props) => {

    const classes = smallCardsStyles();

    return (
        
        
            <div className={classes.root}>
                
                <Box className={classes.topSection} >
                    <Box>

                        <Typography className={classes.smallCardNum}>{props?.data?.number}</Typography>
                    </Box>
                    <Box>
                        <Avatar src={props?.data?.image}></Avatar>
                    </Box>


                </Box>
                <Box className={classes.bottomSection}>
                    <Typography className={classes.smallCardText}>{props?.data?.title}</Typography>
                </Box>
                </div>
            

        
    );
}
