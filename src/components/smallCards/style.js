import { makeStyles } from "@mui/styles";
import { height } from "@mui/system";

export const smallCardsStyles = makeStyles((theme) => ({
    card:{
        // backgroundColor:"red",
        // marginBottom:"14px",
    },
    smallCardNum:{
        color:"#091B29",
        fontSize: "24px",
    },
    smallCardText:{
        color:"#091B29",
        fontSize:"14px",
    },
    SmallCardsRow:{
        justifyContent:"space-evenly",
        textAlign:"center",
        alignItems:"center",
    },
    root:{
        // width:"142px",
        // height:"113px",
        // marginBottom:"15px",
        backgroundColor: "white",
        borderRadius: "4px",
        padding: "12px",
        height: "112px",
        boxShadow:"0px 3px 30px #5C86CB2E"
    },
    rightIcon:{
        width:"18px",
        height:"24px",
    },
    topSection:{
        display:"flex",
        alignItems:"center",
        justifyContent:"space-between",
        paddingBottom:"8px",
    },
    bottomSection:{
        width:"50%"
    }
    

}));