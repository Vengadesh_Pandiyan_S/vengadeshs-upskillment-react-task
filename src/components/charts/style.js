import { makeStyles } from "@mui/styles";

export const chartStyles = makeStyles((theme) => ({
    card:{
        // backgroundColor:"red",
        // width:"100%"
    },
    bigCardNum:{
        color:"#091B29",
        fontSize: "24px",
    },
    headTitle:{
        color:"#091B29",
        fontSize:"16px",
        fontWeight:"600",
        fontFamily:"sans-serif",
    },
    BigCardsRow:{
        display:"flex",
        justifyContent:"space-evenly",
        textAlign:"center",
        alignItems:"center",
    },
    dataNumber:{
        color:"#4E5A6B",
        fontSize:"16px",
    },
    dataText:{
        color:"#98A0AC",
        fontSize:"12px",
        textAlign:"left",
    }
    

    

}));