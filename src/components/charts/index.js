import React from 'react';
// import {AppBar, Toolbar, IconButton, Typography, Drawer} from '@mui/material';
import { Box,Card, CardActions, CardContent, Grid, Typography } from '@mui/material';
import { bigCardsStyles } from './style';
import ReactApexChart from 'apexcharts';





export const BigCards = (props) => {

    const classes = chartStyles();
    const data = {
          
        series: [44, 55, 41, 17, 15],
        options: {
          chart: {
            type: 'donut',
          },
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        },
      
      
      };
    
    return (
        <div className={classes.card}>
                <Grid>
                    {/* {
                        props?.gokul && <Typography>Hi yyugu</Typography>

                    } */}
                    
                    <Card className={classes.root}>
                        <CardContent>
                            <Grid container className={classes.BigCardsRow}>
                                <Grid item xs={6}>
                                    <Typography className={classes.headTitle}>{props?.data?.headTitle}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    {/* <Box className={classes.headIcon}>{props?.data?.headIcon}</Box> */}
                                    {/* <CloseIcon /> */}
                                    <Box><img src='images/icons8-enlarge-48@2x.png'></img></Box>
                                </Grid>
                            </Grid>
                            {/* {
                                props?.pieChart && 
                                <div id="chart">
                                <ReactApexChart options={data?.options} series={data?.series} type="donut" />
                              </div>                             
                            } */}
                            
                            

                            {/* <Typography className={classes.bigCardText}>{props?.data?.title}</Typography> */}
                            <Grid container>
                                <Grid item xs={6}>
                                    <Typography className={classes.dataNumber}>{props?.data?.vacantValue}</Typography>
                                    <Typography className={classes.dataText}>{props?.data?.vacantText}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography className={classes.dataNumber}>{props?.data?.occupiedValue}</Typography>
                                    <Typography className={classes.dataText}>{props?.data?.occupiedText}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography className={classes.dataNumber}>{props?.data?.reservedValue}</Typography>
                                    <Typography className={classes.dataText}>{props?.data?.reservedText}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography className={classes.dataNumber}>{props?.data?.listedValue}</Typography>
                                    <Typography className={classes.dataText}>{props?.data?.listedText}</Typography>
                                </Grid>
                            </Grid>

                        </CardContent>
                        <CardActions>

                        </CardActions>
                    </Card>
                </Grid>

            
        </div>
    );
}
