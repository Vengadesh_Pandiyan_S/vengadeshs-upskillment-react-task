import { makeStyles } from "@mui/styles";

export const cardStyles = makeStyles((theme) => ({
   root: {
      backgroundColor: "red",
      borderRadius: "4px",
      padding: "12px",
      height: (props) => props?.height ?? "100%"
   }
}))