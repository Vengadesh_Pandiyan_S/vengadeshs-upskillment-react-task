import { Avatar, Box, Typography } from '@mui/material'
import React from 'react'
import { cardStyles } from './style'
export const DashboardCard = (props) => {
    const classes = cardStyles()
    return (
        <>
            <div className={classes.root}>
                <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"}>
                    <Box>

                        <Typography>{props?.data?.number}</Typography>


                    </Box>
                    <Box>
                        <Avatar></Avatar>
                    </Box>


                </Box>
                <Box>
                    <Typography>{props?.data?.title}</Typography>
                </Box>
            </div>
        </>
    )
}