import React from 'react';
import { Box, MenuItem, Select, TextField, Typography } from '@mui/material';
import { inputFieldStyles } from './style';





export const InputField = ({
    label = "",
    placeholder = "",
    onChange = ()=>false,
    value = "",
    errorMsg="",
    error="",
    required=false
    


}) => {

    const classes = inputFieldStyles();
    return (
        <div className={classes.root}>
              <Typography className={classes.labelText}>{label} {required&&
              <span className={classes.errorMsg}>*
              </span>}
            </Typography>
            <TextField
                className={classes.textField}
                // id="outlined-basic" 
                variant="outlined" 
                value={value}
                onChange={onChange}
                fullWidth
                size="small"
            />
            {error&&
            <Typography className={classes.errorMsg}> {errorMsg}</Typography>
}
        </div>



    );
}


