import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
   tableParent:{
    boxShadow:"none !important",
    // height: "368px",
    "&::-webkit-scrollbar": {
        display:"none"
        },
   },
   tableHeader:{
    backgroundColor:"#F5F7FA",
    "& th":{
        color:"#4E5A6B",
        fontSize:"12px !important",
        fontWeight:"bold",
        padding:"10px 14px",
        whiteSpace: "nowrap"
    },
   },
   tableBody:{
    "& td":{
        color:"#091B29",
        fontSize:"14px !important",
        fontWeight:"bold",
        padding:"16px 16px",
        whiteSpace: "nowrap"
    }
   },
   active:{
    color:"#5AC782",
    fontSize:"14px",
    
   },
   inactive:{
    color:"#CED3DD",
    fontSize:"14px",
   },
   delete:{
    color: "#FF4B4B", 
    fontSize: "14px",
   },
   appartment:{
    color: "#FFFFFF", 
    backgroundColor: "#78B1FE", 
    padding: "4px", 
    borderRadius: "6px", 
    fontSize:"12px",
   }
   
}))