import React from 'react';
import { Box, Table, TableBody, TableContainer, TableHead, TableRow, Paper, Typography, IconButton, MenuItem, Menu } from '@mui/material';
import { useStyles } from "./style";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { styled } from "@mui/material/styles";
import { ThreeDotsIcon } from '../../assets';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    color: "#4E5A6B",
    fontSize: "12px",
    fontFamily: "tenant_semiBold",
    border: 0,
    backgroundColor: "#F2F4F7",
    fontFamily: "crayond_regular",
    padding: "14px 10px",
    position: "sticky",
    fontWeight: "600px",
  },
  [`&.${tableCellClasses.body}`]: {
    color: "#091B29",
    fontSize: "14px",
    fontFamily: "crayond_regular",
    borderBottom: "2px solid #F2F4F7",
    whiteSpace: "normal",
    wordWrap: "break-word",
    padding: "18px 10px",
    fontWeight: "600px",



  },
}));

export const CustomizeTable = ({
  // val = {},
  heading = [],
  rows = [],
  dataType = [],
  onClick = () => false,
  tableType = "",
  handleIcon = () => false,
  height=500,
}) => {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [index, setIndex] = React.useState(null);
  const open = Boolean(anchorEl)

  const handleClose = () => {
    setAnchorEl(null)
  }
  const getComponentType = (val, data) => {
    // console.log(data)
    const main = data.main;

    switch (data.type) {
      case "status": {
        if (data.value === "Approved") {
          return <p style={{ color: "#267C24" }}>{data.value}</p>;
        } else if (data.value === "Rejected") {
          return <p style={{ color: "red" }}>{data.value}</p>;
        } else if (data.value === "Pending") {
          return <p style={{ color: "#78B1FE" }}>{data.value}</p>;
        } else if (data.value === "Cancelled") {
          return <p style={{ color: "#98A0AC" }}>{data.value}</p>;
        } else if (data.value === "Delete") {
          return <p className={classes.delete}>{data.value}</p>;
        }
        else if (data.value === "Appartment") {
          return <span className={classes.appartment}>{data.value}</span>;
        } else if (data.value === "Quote Accepted") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#5AC782",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data?.value === "open") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#5AC782",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "close" || data?.value === "closed") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#CED3DD",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data?.value === "Closed") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "red",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
                padding: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data?.value === "Open") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#5AC782",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
                padding: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "requested for visit") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#FF9340",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "requested for re-quote accepted") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#FF9340",
                fontSize: "10px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "inProgress") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#78B1FE",
                fontSize: "12px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "In Progress") {
          return (
            <Typography
              sx={{
                color: "white",
                backgroundColor: "#78B1FE",
                fontSize: "12px",
                fontFamily: "tenant_bold",
                textAlign: "center",
                borderRadius: "4px",
              }}
            >
              {data.value}
            </Typography>
          );
        } else if (data.value === "Active") {
          return (
            <Typography className={classes.active}>
              {data.value}
            </Typography>
          );
        } else if (data.value === "Inactive") {
          return <Typography className={classes.inactive}>{data.value}</Typography>;
        } else if (data.value === "delete") {
          return (
            <div>
              {/* <IconButton onClick={() => props.handleIcon("delete", main)}>
                    <DeleteForeverIcon color="primary" />
                  </IconButton> */}
            </div>
          );
        } else {
          return data.value;
        }

      }

      case "propertyType": {
        return (
          <>
            {
              data.value &&
              <Typography
                sx={{
                  color: "white",
                  backgroundColor: "#78B1FE",
                  fontSize: "12px",
                  fontFamily: "tenant_bold",
                  textAlign: "center",
                  borderRadius: "4px",
                  display: "inline-block",
                }}
              >
                &nbsp;&nbsp;&nbsp;{data.value}&nbsp;&nbsp;&nbsp;
              </Typography>
            }
          </>

        );
      }

      case "more": {
        return (
          <div >
            <IconButton onClick={(e) => {
              setAnchorEl(e.currentTarget);
              setIndex(data.index)
            }}>
              {/* <MoreVertIcon className={classes.more} /> */}
              <ThreeDotsIcon />
            </IconButton>

            {index === data?.index &&
              <Menu
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                className={classes.menuList}
              >
                <MenuItem onClick={() => {
                  handleClose()
                  handleIcon("edit", main)
                }} className={classes.menuItem}>Edit</MenuItem>
                <MenuItem onClick={() => {
                  handleClose()
                  handleIcon("active", main, main.status)
                }} className={classes.menuItem}>{main.status === "Active" ? "In-active" : "Active"}</MenuItem>

                <MenuItem onClick={() => {
                  handleClose()
                  handleIcon("delete", main)
                }} className={classes.menuItem}>Delete</MenuItem>

              </Menu>
            }
          </div >
        )
      }

      default: {
        return data.value;
      }
    }
  };


  return <Box className={classes.root} sx={{ height: "100%" }} >
    <TableContainer
      // sx={{
      //     minHeight:`calc(100vh -  300px)`,
      //     maxHeight:`calc(100vh -  300px)`,
      //     overflow:"auto"
      // }}
      sx={{ maxHeight: height }}
      component={Paper} className={classes.tableParent}>
      <Table  stickyHeader aria-label="sticky table" >
        <TableHead className={classes.tableHeader}>

          {heading?.length > 0 && (
            <TableRow>
              {heading?.map((data, index) => {
                return (
                  <StyledTableCell size="small" key={index}>
                    {data.title}
                  </StyledTableCell>

                );
              })}
            </TableRow>
          )}
        </TableHead>
        <TableBody className={classes.tableBody}>

          {rows?.length > 0 && (
            <>
              {rows?.map((row, index) => {
                console.log(row);
                return (
                  <>
                    <TableRow
                      key={index + 1}
                      className={classes.tbody}
                      onClick={() => {
                        onClick && onClick(row)
                      }}
                    >
                      {dataType.map((val) => {

                        return (
                          <StyledTableCell component="th" scope="row"
                            onClick={() => {
                              if (val.type[0] === "more") {

                              } else {
                                handleIcon("view", row)
                              }
                            }}>
                            {val.type.map((type) => {
                              return getComponentType(row, {
                                index: index,
                                type: type,
                                main: row,
                                value: row[val.name],
                                image: row[val.imagekey],
                                icon: row[val.icon],
                                is_active: row[val.is_active],
                                objectOption: val?.label ?? "label",
                              });
                            })}
                          </StyledTableCell>

                        );
                      })}
                    </TableRow>
                    {tableType === "normal" && <Box height={"8px"} />}
                  </>
                );
              })}
            </>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  </Box>

}