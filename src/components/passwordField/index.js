import React, {useState} from 'react';
import { Box, IconButton, InputAdornment, MenuItem, OutlinedInput, Select, TextField, Typography } from '@mui/material';
import { passwordFieldStyles } from './style';
import { Visibility, VisibilityOff } from '@mui/icons-material';





export const PasswordField = (props) => {
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
      });
    
      const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
      };
    
      const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };

    const classes = passwordFieldStyles();

    return (
        <div className={classes.root}>
            <OutlinedInput
                id="outlined-adornment-password"
                type={values.showPassword ? 'text' : 'password'}
                // value={props?.value}
                // onChange={props?.handleEvent('password')}
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                        >
                            {values.showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                }
            />
        </div>



    );
}
