import React from 'react';
import { Box,  IconButton,  Typography } from '@mui/material';
import { reruestCardStyles } from './style';





export const RequestCard = (props) => {

    const classes = reruestCardStyles();

    return (
        <div className={classes.root}>
            <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"}>
                <Box>
                    <Typography className={classes.requestTitle}>{props?.data?.title}</Typography>
                    <Typography className={classes.requestName} display={"flex"} alignItems={"center"}>{props?.data?.requestName} . {props?.data?.date} . {props?.data?.requestId}</Typography>
                </Box>
                <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"}>
                    <IconButton><img src='/images/EditIcon.svg' /></IconButton>
                    <IconButton><img src='/images/ViewIcon.svg' /></IconButton>
                </Box>

            </Box>
        </div>



    );
}
