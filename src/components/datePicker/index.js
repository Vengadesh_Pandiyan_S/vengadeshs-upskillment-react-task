import React from 'react';
import { Box, TextField, Stack, Typography } from '@mui/material';
import { useStyles } from "./style";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

export const DateShower = ({
    label = "Field",
    placeholder = "",
    onChange = ()=>false,
    value = "",
    errorMsg="",
    error="",
    required=false
    


}) => {

    const classes = useStyles();
    

    return (
        <>
        <Box className={classes.root}>
        <Stack>
            <label className={classes.labelText}>{label ?? ""} {required&&
              <span className={classes.errorMsg}>*
              </span>}</label>
            
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                    className={classes.dateStyling}
                    value={value}
                    onChange= {onChange}
                    renderInput={(params) => <TextField {...params} />}
                />
            </LocalizationProvider>
            {error &&
            <Typography className={classes.errorMsg}> {errorMsg}</Typography>
}
        </Stack>
    </Box>
    </>
    )

}