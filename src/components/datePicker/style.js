import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    label:{
        color:"#98A0AC",
        fontSize:"12px",
        marginBottom:"8px"
    },
    dateStyling:{
        "& .css-9rcej8-MuiInputBase-root-MuiOutlinedInput-root":{
            borderRadius:"10px",
            color:"#091B29",
            fontSize:"14px"
        },
        "& .css-i4bv87-MuiSvgIcon-root":{
            width:"20px",
            height:"20px"
        },
        "& .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input":{
            padding: "8.5px 14px"
        }
    },
    errorMsg:{
        color:"red", 
        fontSize:"12px",
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },


}))