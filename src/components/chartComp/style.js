import { makeStyles } from "@mui/styles";


export const inputFieldStyles = makeStyles((theme) => ({
    root: {
        // width:"100%"
    },
    textField: {
        // backgroundColor:"white",
        // borderRadius:"10px",
        // border:"1px solid #E4E8EE",
        // outline: "none",
        // "& .MuiAutocomplete-inputRoot": {
        //     "& input": {
        //       padding: "0.5px 4px !important",
        //     },
        //   },
        //   "& .MuiAutocomplete-tag": {
        //     margin: "0px",
        //     height: "20px",
        //     borderRadius: "5px",
        //     marginLeft: "5px",
        //     "& .MuiChip-deleteIcon": {
        //       width: "17px",
        //     },
        //   },

        "& .MuiTextField-root" :{
            borderRadius:"8px",
            "& input":{
                padding:"10px 14px"
            }
        }

    },
    errorMsg:{
        color:"red", 
        fontSize:"12px",

    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },



}));