import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
//  {* -------------mapping style-------------*} */}
const style = {
    height: "100%"
}
class Mappping extends Component {
    render() {
        return (
            // {* -------------mapping component-------------*}
            <Map
                zoom={16}
                google={this.props.google}
                style={style}
                //   initialCenter={{ lat:  this.props.latlog.lat , lng: this.props.latlog.log}}
                initialCenter={{ lat: 13.072090, lng: 80.201859 }}
                
      
                // center={{
                //     lat: this.props.latlog.lat,
                //     lng: this.props.latlog.log
                //   }}
                options={{  // <-- Use this
                    gestureHandling: "none", zoomControl: false
                }}
            >
                <Marker
                    title={'The marker`s title will appear as a tooltip.'}
                    name={'SOMA'}
                    position={{ lat: 13.072090, lng: 80.201859  }}
                />
                <Marker
                    name={'Dolores park'}
                    position={{ lat: 13.072090, lng: 80.201859  }} />
                <Marker />
            </Map>
        );
    }
}
export default GoogleApiWrapper({
    // {* -------------mapping access key-------------*} */}
    // this.props.apiKey
    apiKey: ('AIzaSyCA_KEgg4J5A3iQp435wTIGt5RR7Gb6fos'),
    version: 3.31
})(Mappping);