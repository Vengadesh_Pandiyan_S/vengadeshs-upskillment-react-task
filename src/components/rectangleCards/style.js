import { makeStyles } from "@mui/styles";


export const rectangleCardsStyles = makeStyles((theme) => ({
    card:{
        // backgroundColor:"red",
        // marginBottom:"14px"
    },
    rectangleCardText:{
        color:"#091B29",
        fontSize: "14px",
        fontFamily:"sans-serif",
        textAlign:"left",
    },
    rectangleCardVal:{
        color:"#091B29",
        fontSize:"18px",
        fontFamily:"sans-serif",
        fontWeight:"600",
    },
    SmallCardsRow:{
        justifyContent:"space-between",
        textAlign:"center",
        alignItems:"center",
        display:"flex"
    },
    root:{
        padding:"12px",
        height:"50px",
        backgroundColor:"white",
        borderRadius:"4px",
        boxShadow:"0px 3px 30px #5C86CB2E"
    },
    rightIcon:{
        width:"18px",
        height:"24px"
    }
    

}));