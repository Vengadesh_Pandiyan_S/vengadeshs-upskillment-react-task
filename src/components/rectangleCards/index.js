import React from 'react';
import { Box,  Typography } from '@mui/material';
import { rectangleCardsStyles } from './style';





export const RectangleCards = (props) => {

    const classes = rectangleCardsStyles();

    return (
        <div className={classes.root}>
            <Box className={classes.SmallCardsRow}>
                <Box>
                    <Typography className={classes.rectangleCardText}>{props?.data?.area}</Typography>
                </Box>
                <Box>
                    <Typography className={classes.rectangleCardVal}>{props?.data?.areaVal} sq.m</Typography>
                </Box>

            </Box>
        </div>



    );
}
