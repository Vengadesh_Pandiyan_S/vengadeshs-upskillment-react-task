import { makeStyles } from "@mui/styles";
const drawerWidth = 56;
export const useStyles = makeStyles((theme) => ({
    root: {
        width: props => props?.isMobile ? 240 : drawerWidth,
        position: 'absolute',
    },
    drawer: {
        height:  `calc(100vh - 58px)`,
        // width: props => props?.isMobile ? 240 : drawerWidth,
        width: "72px",
        backgroundColor:"#333333",
        position:"absolute",
        // marginTop:"57px",
        // top: "64px",
    },
    drawerContainer: {
        // overflow: 'hidden',
        // color:"red"
    },
    navIcons:{
        color:"#C1C5CB",
    },
    openDrawer:{
        
    },
    
}))