
import React from 'react';
import { useStyles } from "./style";
import { Paper, List, ListItem, ListItemIcon, ListItemText, Typography, Stack, Divider, appBarClasses } from '@mui/material';
import { matchPath, useNavigate, useLocation, Routes } from 'react-router-dom';
import { Leads } from "../../../assets/leads";
import { DownArrow } from '../../../assets/downArrow';
import { FourBoxIcon } from '../../../assets/fourBoxIcon';
import { AppRoutes } from '../../../router/routes';
import { PropertyCreation } from '../../../screens/propertyCreation/propertyCreation';
import GroupAddTwoToneIcon from '@mui/icons-material/GroupAddTwoTone';
import PreviewIcon from '@mui/icons-material/Preview';
import TableViewIcon from '@mui/icons-material/TableView';
import { Box } from '@mui/system';

export const SideNavBar = (props) => {

    const classes = useStyles(props);

    const navigate = useNavigate();
    const location = useLocation();

    const isSelected = (data) => {
        
        if (data?.link) {
            return location.pathname=== data?.link
        }
    }

    const onListClick = (data) => {
        if (data.link) {
            navigate(data.link)
        }
    }

    const fourBoxIcon = (e) => {
        navigate(AppRoutes.propertiesDash);
    }

    const leads = (e) => {
        navigate(AppRoutes.propertyCreation);
    }

    // if (Routes === PropertyCreation) {
    //     return(
    //         <FourBoxIcon />        
    //     )

    // }

    return (
        <div className={classes.root}>
            <Paper
                className={classes.drawer}
                square
            >
                <div className={classes.drawerContainer}>
                    <List>
                        {/* <Box className={classes.openDrawer}>
                            <FourBoxIcon />
                        </Box> */}
                        {[
                            // {
                            //     icon:<FourBoxIcon />,
                            //     name:"",

                            // },
                            {
                                icon: <FourBoxIcon />,
                                name: "Dashboard",
                                link: "/propertiesDash"
                            },
                            {
                                icon: <GroupAddTwoToneIcon />,
                                name: "propertyCreation",
                                link: AppRoutes.propertyCreation,
                            },
                            {
                                icon: <PreviewIcon />,
                                name: "View",
                                link: "/propertyView"
                            },
                            {
                                icon: <TableViewIcon />,
                                name: "propertyTable",
                                link: AppRoutes.propertyTable,                                
                            }
                        ].map((navBar, index) => (
                            <ListItem onClick={(e) => onListClick(navBar)}
                                button
                                key={index}
                                selected={isSelected(navBar)}>

                                <ListItemIcon className={classes.navIcons}>{navBar.icon}</ListItemIcon>

                                {/* <ListItemText primary={navBar.name} /> */}

                            </ListItem>
                        ))}
                    </List>
                </div>
            </Paper>
        </div>
    );
}



