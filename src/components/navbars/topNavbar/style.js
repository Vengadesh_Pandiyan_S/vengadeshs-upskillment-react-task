import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
        zIndex: theme.zIndex.drawer + 1,
        
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        backgroundColor:"#1C1C1C",
        position:"relative"
    },
    toolbarHeight:{
        minHeight:"57px",
        paddingLeft: "8px"    
    },
    title: {
        display: 'block',
    },
    menuIcon: {
        [theme.breakpoints.up("md")]: {
            display: "none"
        }
    },
    navbarTopLeftDropdown :{
        [theme.breakpoints.down("sm")]: {
            display: "none"
        }
    },
    
    logoTop:{
        width:"115px",
    },
    management:{
        fontSize:"10px"
    },
    adminName:{
        fontSize:"12px"
    },
    adminOccupation:{
        fontSize:"10px"
    }

}))