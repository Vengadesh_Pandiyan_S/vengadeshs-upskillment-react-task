import React from 'react';
import { Box, MenuItem, Select, Typography } from '@mui/material';
import { selectFieldStyles } from './style';





export const SelectField = ({
    label = "Field",
    placeholder = "",
    onChange = () => false,
    value = "",
    errorMsg = "",
    error = "",
    required = false,
    options = [
        {
            label: "",
            value: "",
        }
    ],



}) => {

    const classes = selectFieldStyles();

    return (
        <div className={classes.root}>
            <Typography className={classes.labelText}>{label} {required &&
                <span className={classes.errorMsg}>*
                </span>}
            </Typography>
            <Select
                fullWidth
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={value}
                // label="Age"
                onChange={onChange}
                size="small"
            >
                {
                    options?.map((item) => {
                        return (
                            <MenuItem value={item?.value}>{item?.label}</MenuItem>
                        )
                    })
                }

            </Select>
            {error &&
                <Typography className={classes.errorMsg}>{errorMsg}</Typography>
            }
        </div>



    );
}
