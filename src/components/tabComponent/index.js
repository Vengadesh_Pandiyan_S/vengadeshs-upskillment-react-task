// import React from 'react';
// import { Avatar, Box, Card, CardContent, Grid, Stack, Tab, Typography, } from '@mui/material';
// import { tabStyles } from './style';
// import TabContext from '@mui/lab/TabContext';
// import TabList from '@mui/lab/TabList';
// import TabPanel from '@mui/lab/TabPanel';





// export const Tabs = (props) => {
//     const [value, setValue] = React.useState('1');

//     const handleChange = (event, newValue) => {
//       setValue(newValue);
//     };

//     const classes = tabStyles();

//     return (

//         <div className={classes.root}>
//             <Box sx={{ width: '100%', typography: 'body1' }}>
//                 <TabContext value={value}>
//                     <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
//                         <TabList onChange={handleChange} aria-label="lab API tabs example">
//                             <Tab label="Item One" value="1" />
//                             <Tab label="Item Two" value="2" />
//                             <Tab label="Item Three" value="3" />
//                         </TabList>
//                     </Box>
//                     <TabPanel value="1">Item One</TabPanel>
//                     <TabPanel value="2">Item Two</TabPanel>
//                     <TabPanel value="3">Item Three</TabPanel>
//                 </TabContext>
//             </Box>

//         </div>



//     );
// }


import React from 'react'
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { tabStyles } from './style';
// const useStyles = makeStyles((theme) => ({
   
// }));
export const DataTabs = ({ tabs = [], activeTab, handleChangeTab }) => {
    const classes = tabStyles();
    const handleChange = (event, newValue) => {
        handleChangeTab(newValue);
    };
    return (
        <div>
            <Box sx={{ width: '100%', typography: 'body1' }}>
                <TabContext value={activeTab} >
                    <Box sx={{ borderBottom: 1, borderColor: 'divider', margin: "2px 0px", padding: "0px 30px" }}>
                        <TabList className={classes.underTab} onChange={handleChange} aria-label="lab API tabs example">
                            {
                                tabs?.map(tab => (
                                    <Tab className={classes.tab} label={tab.label} value={tab.value} />
                                ))
                            }
                        </TabList>
                    </Box>
                    <TabPanel className={classes.TabPanel} value={activeTab}>{tabs?.find(tab => tab.value === activeTab)?.body}</TabPanel>
                </TabContext>
            </Box>
        </div>
    )
}
