import { makeStyles } from "@mui/styles";


export const tabStyles = makeStyles((theme) => ({

    tab: {
        '&.Mui-selected': {
            color: '#000',
        },
    },
    underTab: {
        "& .MuiTabs-indicator": {
            backgroundColor: "#0065E6",
            height: "3px",
        }
    },
    TabPanel: {
        padding: "10px 24px !important",
    }

}));