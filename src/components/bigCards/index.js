import React from 'react';
import { Avatar, Box, Card, Grid, Typography } from '@mui/material';
import { bigCardsStyles } from './style';
// import ReactApexChart from 'apexcharts';





export const BigCards = (props) => {

  const classes = bigCardsStyles(props);
  const data = {

    series: [44, 55, 41, 17, 15],
    options: {
      chart: {
        type: 'donut',
      },
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    },


  };

  return (
      <div className={classes.root}>
        <Box className={classes.headSection} >
          <Box>
            <Typography className={classes.headTitle}>{props?.data?.headTitle}</Typography>
          </Box>
          <Box>
            <Avatar className={classes.rightIcon} src={props?.data?.icon}></Avatar>
            {/* <img src={props?.data?.icon}/> */}
          </Box>


        </Box>
        <Box className={classes.chartSection}>
          {/* {
            props?.chart && <Typography>Chart Will be Appear Here</Typography>
          } */}
          {/* <Avatar className={classes.chartImage} src={props?.data?.image}></Avatar> */}
          <img className={classes.chartImage} src={props?.data?.image} />
        </Box>
        <Grid container className={classes.labelSection}>
          <Grid item xs={6}>
            <Typography className={classes.dataNumber}>{props?.data?.vacantValue}</Typography>
            <Typography className={classes.dataText}>{props?.data?.vacantText}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.dataNumber}>{props?.data?.occupiedValue}</Typography>
            <Typography className={classes.dataText}>{props?.data?.occupiedText}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.dataNumber}>{props?.data?.reservedValue}</Typography>
            <Typography className={classes.dataText}>{props?.data?.reservedText}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.dataNumber}>{props?.data?.listedValue}</Typography>
            <Typography className={classes.dataText}>{props?.data?.listedText}</Typography>
          </Grid>
        </Grid>

      </div>

  );
}
