import { makeStyles } from "@mui/styles";

export const bigCardsStyles = makeStyles((theme) => ({
    card:{
        // backgroundColor:"red",
        // width:"100%"
    },
    bigCardNum:{
        color:"#091B29",
        fontSize: "24px",
    },
    headTitle:{
        color:"#091B29",
        fontSize:"16px",
        fontWeight:"600",
        fontFamily:"sans-serif",
    },
    BigCardsRow:{
        display:"flex",
        justifyContent:"space-evenly",
        textAlign:"center",
        alignItems:"center",
    },
    dataNumber:{
        color:"#4E5A6B",
        fontSize:"16px",
    },
    dataText:{
        color:"#98A0AC",
        fontSize:"12px",
        textAlign:"left",
    },
    root:{
        // height:"370px",
        // width:"300px",
        // marginBottom:"15px"
        backgroundColor: "white",
        borderRadius: "4px",
        padding: "12px",
        height:"100% !important",
        boxShadow:"0px 3px 30px #5C86CB2E"
    },
    headSection:{
        display:"flex",
        alignItems:"center",
        justifyContent:"space-between",
        paddingBottom:"16px",

    },
    chartImage:{
        height:"100%",
        width:"100%",
        margin:"auto"
    },
    rightIcon:{
        width:"18px",
        height:"18px"
    },
    labelSection:{
        paddingTop:"22px"
    }
    

    

}));