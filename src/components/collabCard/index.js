import React from 'react';
import { Avatar, AvatarGroup, Box, Button, InputLabel, Menu, MenuItem, Select, Typography } from '@mui/material';
import { collabCardsStyles } from './style';
import TourIcon from '@mui/icons-material/Tour';
import StarRateRoundedIcon from '@mui/icons-material/StarRateRounded';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import Divider from '@mui/material/Divider';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import QuestionAnswerOutlinedIcon from '@mui/icons-material/QuestionAnswerOutlined';
import { CircularProgressbar } from 'react-circular-progressbar';
// CircularProgressbarWithChildren, buildStyles





export const CollaboCards = (props) => {

    const classes = collabCardsStyles();

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };


    const percentage = 66;

    return (
        <div className={classes.root}>
            {/* first Section */}
            <Box className={classes.topSection} >
                <Box className={classes.dateSection}>
                    <Typography className={classes.date}>{props?.data?.date}</Typography>
                </Box>
                <Box className={classes.middleSection}>
                    <Box className={classes.tourIcon}>

                        <TourIcon className={classes.flagIcon} />
                    </Box>
                    <Box className={classes.overdue}>
                        <Typography className={classes.overdueText}>Overdue!</Typography>
                    </Box>
                </Box>
                <Box className={classes.iconsSection}>
                    <Box className={classes.rightIconsSection}>
                        <Box className={classes.infoIconSection}>
                            <InfoOutlinedIcon className={classes.infoIcon} />
                        </Box>
                        <Box className={classes.starIconSection}>
                            <StarRateRoundedIcon className={classes.starIcon} />
                        </Box>
                    </Box>
                    <Box className={classes.vertIconSecion} textAlign={"right"}>
                        <MoreVertIcon className={classes.vertIcon} />
                    </Box>
                </Box>

            </Box>

            {/* second Section */}

            <Typography className={classes.title}>{props?.data?.title}</Typography>


            {/* card Section */}
            <Box className={classes.thirdSection}>
                <Box className={classes.cards}>
                    <Typography className={classes.cardTexts}>{props?.data?.leftCard}</Typography>
                </Box>
                <Box className={classes.cards}>
                    <Typography className={classes.cardTexts}>{props?.data?.rightCard}</Typography>
                </Box>

            </Box>

            {/* fourthSection */}
            <Box className={classes.fourthSection}>
                <Box className={classes.fourthLeftSection}>


                    <TourIcon className={classes.flagIcon} />



                    {/* <CircleOutlinedIcon className={classes.circleOutlinedIcon} /> */}
                    <Box label="Default">
                        <CircularProgressbar value={percentage} text={`${percentage}%`} />
                    </Box>


                    <Typography className={classes.completed}>{props?.data?.completed}</Typography>

                </Box>
                <Box className={classes.fourthRightSection}>
                    <Button
                        id="fade-button"
                        aria-controls={open ? 'fade-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                        variant="contained"
                    >
                        Dashboard
                    </Button>
                    <Menu
                        id="fade-menu"
                        MenuListProps={{
                            'aria-labelledby': 'fade-button',
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        // TransitionComponent={Fade}
                    >
                        <MenuItem onClick={handleClose}>Pending</MenuItem>                        
                        <MenuItem onClick={handleClose}>Yes</MenuItem>
                        <MenuItem onClick={handleClose}>No</MenuItem>
                        <MenuItem onClick={handleClose}>May Be</MenuItem>
                    </Menu>
                </Box>
            </Box>

            <Divider />

            <Box className={classes.lastSection}>
                <Box className={classes.bottomLeftSection} >
                    <AvatarGroup max={3} sx={{ float: "left" }} >
                        <Avatar sx={{ width: 34, height: 34 }} alt="Remy Sharp" src="" />
                        <Avatar sx={{ width: 34, height: 34 }} alt="Travis Howard" src="" />
                        <Avatar sx={{ width: 34, height: 34 }} alt="Cindy Baker" src="" />
                        <Avatar sx={{ width: 34, height: 34 }} alt="Agnes Walker" src="" />
                        <Avatar sx={{ width: 34, height: 34 }} alt="Trevor Henderson" src="" />
                    </AvatarGroup>



                    <Box className={classes.estSection}>
                        <AccessTimeIcon className={classes.clockIcon} />
                        <Typography className={classes.estText}>Est.{props?.data?.estHrs}</Typography>
                    </Box>

                </Box>
                <Box className={classes.bottomRightSection}>
                    <Typography className={classes.discussSection}>
                        Discuss
                    </Typography>
                    <QuestionAnswerOutlinedIcon className={classes.discussIcon} />

                </Box>


            </Box>




        </div>



    );
}
