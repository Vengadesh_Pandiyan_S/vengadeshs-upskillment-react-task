import { makeStyles } from "@mui/styles";
import { display, fontSize, fontWeight } from "@mui/system";



export const collabCardsStyles = makeStyles((theme) => ({
   root:{
      backgroundColor:"white",
      boxShadow:"0px 3px 30px #5C86CB2E",
      padding:"12px",
      borderRadius:"12px",
   },
   title:{
      color:"#172b4d",
      marginBottom:"8px",
      fontFamily:"sans-serif",
      fontSize:"16px"

   },
   topSection:{
      // display={"flex"} alignItems={"center"} justifyContent={"space-between"}
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",
   },
   middleSection:{
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",
   },
   rightIconsSection:{
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",
   },
   date:{
      color:"gray",
      fontSize:"10px",
      fontFamily:"sans-serif",
   },
   overdueText:{
      color:"#e74c3c",
      padding:"2px 8px",
      border:"1px solid #e74c3c",
      backgroundColor:"rgba(231,76,60,.0784313725490196)",
      borderRadius:"4px",
      fontSize:"10px",
   },
   overdue:{
      marginLeft:"6px"
   },
   flagIcon:{
      // color: (props) => props?.color ?? "#f39c12 !important",
      color:"#f39c12",
      // backgroundColor: (props) => props?.backgroundColor ?? "#f39c1214",
      backgroundColor:"#f39c1214",
      padding:"4px",
      borderRadius:"50%",
      fontSize:"20px"
      
   },
   starIcon:{
      // color: (props) => props?.color ?? "#e8e8e8",
      color:"#e8e8e8",
      fontSize:"20px"
      

   },
   infoIcon:{
      // color: (props) => props?.color ?? "#8d8d8d",
      color:"#8d8d8d",
      marginRight:"6px",
      fontSize:"20px"

   },
   vertIcon:{
      // color: (props) => props?.color ?? "#000000a6",
      color:"#000000a6",
      fontSize:"20px"
   },
   thirdSection:{
      display:"flex",
      alignItems:"center",
      // justifyContent:"space-between",

   },
   cards:{
      marginRight:"6px"

   },
   cardTexts:{
      backgroundColor:"#c6c6c63d",
      color:"#535353",
      padding:"2px 8px",
      borderRadius:"4px",
      fontSize:"10px",
      fontFamily:"sans-serif",
      fontWeight:"500",
   },
   fourthSection:{
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",
      marginTop:"24px",
      marginBottom:"12px"

   },
   fourthLeftSection:{
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",   
   },
   // circleOutlined:{
      
   // },
   circleOutlinedIcon:{
      color:"gray",
      fontSize:"20px",
      margin:"0px 6px"
   },
   completed:{
      fontSize:"12px",
      color:"gray",
      fontFamily:"sans-serif",
      fontWeight:"500"
   },
   discussIcon:{
      color:"#00b7a8",
      padding:"4px",
      border:"1px dotted #00b7a8",    
      borderRadius:"50%" 
   },
   discussSection:{
      fontSize:"12px",
      color:"gray",
      fontFamily:"sans-serif",
      fontWeight:"500",
      marginRight:"4px"
   },
   bottomRightSection:{
      display:"flex",
      alignItems:"center"
   },
   estSection:{
      display:"flex",
      alignItems:"center",
      color:"gray", 
      
   },
   clockIcon:{
      fontSize:"20px",
      marginRight:"4px"
   },
   estText:{
      fontSize:"10px"
   },
   bottomLeftSection:{
      display:"flex",
      alignItems:"center",
   },
   lastSection:{
      display:"flex",
      alignItems:"center",
      justifyContent:"space-between",
      marginTop:"12px"
   },
   avatarGroup: {
      "& .MuiAvatar-root": {
          height: "34px",
          width: "34px",
          fontSize: "12px"
      }
  },


    

}));