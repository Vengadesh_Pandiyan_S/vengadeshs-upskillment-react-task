import * as React from "react"

export const AddPropIcon = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={88} height={88} {...props}>
    <defs>
      <filter
        id="a"
        x={0}
        y={0}
        width={88}
        height={88}
        filterUnits="userSpaceOnUse"
      >
        <feOffset dy={4} />
        <feGaussianBlur stdDeviation={4} result="blur" />
        <feFlood floodOpacity={0.078} />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
    </defs>
    <g data-name="Group 102384">
      <g filter="url(#a)">
        <circle
          data-name="Ellipse 129441"
          cx={32}
          cy={32}
          r={32}
          transform="translate(12 8)"
          fill="#5078e1"
        />
      </g>
      <path
        d="M44.982 26a1.1 1.1 0 0 0-1.088 1.122v11.781H32.105a1.104 1.104 0 1 0 0 2.209h11.789v11.782a1.105 1.105 0 0 0 2.211 0V41.112h11.789a1.104 1.104 0 1 0 0-2.209H46.1V27.122a1.105 1.105 0 0 0-1.122-1.12Z"
        fill="#fff"
        stroke="#f3f5f6"
        strokeWidth={0.4}
      />
    </g>
  </svg>
)


