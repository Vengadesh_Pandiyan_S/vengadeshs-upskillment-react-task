import * as React from "react"

const NotificationIcon = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={15.063}
    height={17.5}
    {...props}
  >
    <g data-name="Group 838">
      <path
        d="M6.197 3.506a5.477 5.477 0 0 0-5.126 5.515v2.609L.12 13.544v.011a1.272 1.272 0 0 0 1.137 1.791h3.051a2.154 2.154 0 0 0 4.308 0h3.05a1.273 1.273 0 0 0 1.137-1.791v-.011l-.951-1.914V8.884a5.394 5.394 0 0 0-5.655-5.378Zm.051 1.076a4.3 4.3 0 0 1 4.517 4.3v2.872a.539.539 0 0 0 .056.24l1 2.018a.167.167 0 0 1-.163.255H1.252a.167.167 0 0 1-.162-.255l1-2.017a.539.539 0 0 0 .056-.24V9.021a4.4 4.4 0 0 1 4.101-4.44ZM5.38 15.346h2.154a1.077 1.077 0 1 1-2.154 0Z"
        fill="#fff"
      />
      <g
        data-name="Ellipse 40005"
        transform="translate(7.563 1.5)"
        fill="#5078e1"
        stroke="#071741"
        strokeWidth={1.5}
      >
        <circle cx={3} cy={3} r={3} stroke="none" />
        <circle cx={3} cy={3} r={3.75} fill="none" />
      </g>
    </g>
  </svg>
)

export default NotificationIcon
