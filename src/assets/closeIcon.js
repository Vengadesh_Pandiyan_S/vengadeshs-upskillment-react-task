import * as React from "react"

const CloseIcon = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={48} height={48} {...props}>
    <g data-name="Group 9485" transform="translate(-195 -677.828)">
      <circle
        data-name="Ellipse 850"
        cx={24}
        cy={24}
        r={24}
        transform="translate(195 677.828)"
        fill="#fff0eb"
      />
      <path
        d="M224.628 692.611a.781.781 0 0 0-1.335-.553l-3.125 3.125a.781.781 0 0 0-.228.553v4.365l-1.158 1.156a1.564 1.564 0 1 0 1.1 1.106l1.158-1.158h4.365a.782.782 0 0 0 .553-.228l3.126-3.126a.781.781 0 0 0-.553-1.335h-3.903Zm3.038 8.874a9.377 9.377 0 1 1-8-8l-.6.6a2.344 2.344 0 0 0-.688 1.658 7.033 7.033 0 1 0 7.033 7.033 2.344 2.344 0 0 0 1.657-.688l.6-.6Zm-9.289-4.186a5.47 5.47 0 1 0 5.47 5.47h-2.156l-.2.2a3.126 3.126 0 1 1-3.315-3.315l.2-.2Z"
        fill="#f15a29"
      />
    </g>
  </svg>
)

export default CloseIcon
