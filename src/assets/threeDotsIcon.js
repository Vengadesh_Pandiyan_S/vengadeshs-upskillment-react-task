import * as React from "react"

export const ThreeDotsIcon = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={4} height={16} {...props}>
    <g data-name="Group 96333" transform="translate(-1266 -261)" fill="#98a0ac">
      <circle
        data-name="Ellipse 40396"
        cx={2}
        cy={2}
        r={2}
        transform="translate(1266 261)"
      />
      <circle
        data-name="Ellipse 40397"
        cx={2}
        cy={2}
        r={2}
        transform="translate(1266 267)"
      />
      <circle
        data-name="Ellipse 40398"
        cx={2}
        cy={2}
        r={2}
        transform="translate(1266 273)"
      />
    </g>
  </svg>
)

 
