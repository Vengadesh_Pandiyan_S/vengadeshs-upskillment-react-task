export const Propertypath = ["propertyId", "propertyName", "totalUnits", "occupiedUnits", "occupancy", ]

export const Propertyheading = [
  //{ title: "S No", field: "index", },
  { title: "Property Id", field: "propertyId", },
  { title: "Property Name", field: "propertyName", },
  { title: "Total Units", field: "totalUnits", },
  { title: "Occupied Units", field: "occupiedUnits", },
  { title: "Occupancy", field: "occupancy", },
  
]
export const PropertyFinderType = [
    { type: ["increment"], name: "propertyId" },
    { type: ["text"], name: "propertyName" },
    { type: ["text"], name: "totalUnits" },
    { type: ["text"], name: "occupiedUnits" },
    { type: ["text"], name: "occupancy" },    
  ];


export const Propertyrow = [
    {
      // sno: "",
      propertyId:"Prop 01",
      propertyName: "Rubix Appartment",
      totalUnits: '23',
      occupiedUnits: '02',
      occupancy:"47%",
      
    },
    {
        // sno: "",
        propertyId:"Prop 02",
        propertyName: "Casa Grand",
        totalUnits: '25',
        occupiedUnits: '04',
        occupancy:"20%",
        
      },
      {
        // sno: "",
        propertyId:"Prop 03",
        propertyName: "Appaswamy",
        totalUnits: '20',
        occupiedUnits: '06',
        occupancy:"60%",
        
      },
      {
        // sno: "",
        propertyId:"Prop 04",
        propertyName: "Gold Grand",
        totalUnits: '18',
        occupiedUnits: '08',
        occupancy:"70%",
        
      },
      {
        // sno: "",
        propertyId:"Prop 05",
        propertyName: "Rubix Appartment",
        totalUnits: '23',
        occupiedUnits: '02',
        occupancy:"47%",
        
      },
      {
        // sno: "",
        propertyId:"Prop 06",
        propertyName: "Raghunantham",
        totalUnits: '15',
        occupiedUnits: '04',
        occupancy:"30%",
        
      },
      {
        // sno: "",
        propertyId:"Prop 07",
        propertyName: "Modern Apartment",
        totalUnits: '40',
        occupiedUnits: '20',
        occupancy:"50%",
        
      },




      
  
  ]

export const PropertyTablepath = ["propertyId", "propertyName", "companyName", "location", "revenueType", "status", "propertyType" ]

export const PropertyTableheading = [
  //{ title: "S No", field: "index", },
  { title: "Property Num", field: "propertyId", },
  { title: "Property Name", field: "propertyName", },
  { title: "Company Name", field: "companyName", },
  { title: "location", field: "location", },
  { title: "Revenue Type", field: "revenueType", },
  { title: "Property Type", field: "propertyType", },
  { title: "Status", field: "status", },
  { title: "", field: "icon", },
  
]
export const PropertyTableType = [
    { type: ["increment"], name: "propertyId" },
    { type: ["text"], name: "propertyName" },
    { type: ["text"], name: "companyName" },
    { type: ["text"], name: "location" },
    { type: ["text"], name: "revenueType" },    
    { type: ["status"], name: "propertyType" },    
    { type: ["status"], name: "status" },
    { type: ["more"], name: "icon" },   
  ];


export const PropertyTablerow = [
    {
      // sno: "",
      propertyId:"Prop 01",
      propertyName: "Rubix Appartment",
      companyName: 'Property Automate',
      location: 'T.Nagar, Chennai',
      revenueType:"Sale",
      propertyType:"Appartment",
      status:"Active",
      
    },
    {
        // sno: "",
        propertyId:"Prop 02",
        propertyName: "Casa Grand",
        companyName: 'Crayond',
        location: 'Neelankarai, Chennai',
        revenueType:"Lease",
        propertyType:"Appartment",
        status:"Active",
        
      },
      {
        // sno: "",
        propertyId:"Prop 03",
        propertyName: "Appaswamy",
        companyName: 'Kritilabs',
        location: 'Taramani, Chennai',
        revenueType:"Lease",
        propertyType:"Appartment",
        status:"Inactive",
        
      },
      {
        // sno: "",
        propertyId:"Prop 04",
        propertyName: "Raghunandham",
        companyName: 'Indian Care',
        location: 'Tirunagar, Madurai',
        revenueType:"Sale",
        propertyType:"Appartment",
        status:"Delete",
        
      },
    




      
  
  ]